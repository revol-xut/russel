//
// Created by revol-xut on 04.01.20.
//

#include <boost/test/unit_test.hpp>
#include <string>
#include <nlohmann/json.hpp>

#include "../daemon/network/container/server_container.hpp"
#include "../daemon/network/container/client_container.hpp"
#include "../daemon/network/client.hpp"

using json = nlohmann::json;

template<class T>
void generate_request(const std::string &command, const std::shared_ptr<T> &object, std::shared_ptr<json> &data) {
    auto input = std::make_shared<json>();
    data = std::make_shared<json>();
    (*input)["command"] = "connection_count";
    (*data)["receiver"] = "test";
    object->autoRespond(input, data);
}

BOOST_AUTO_TEST_CASE(test_json_server_container_connection) { //NOLINT
    auto output = std::make_shared<json>();
    auto sever_container = std::make_shared<ServerContainer>(true);
    generate_request<ServerContainer>("connection_count", sever_container, output);
    BOOST_CHECK(output->at("data").at("socket_count").is_number());
    BOOST_CHECK(output->at("data").at("socket_count") == 0);
}

BOOST_AUTO_TEST_CASE(test_json_client_container_connection) { //NOLINT
    auto output = std::make_shared<json>();
    auto sever_container = std::make_shared<ClientContainer>();
    generate_request<ClientContainer>("connection_count", sever_container, output);
    BOOST_CHECK(output->at("data").at("socket_count").is_number());
    BOOST_CHECK(output->at("data").at("socket_count") == 0);
}