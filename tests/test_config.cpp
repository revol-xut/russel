//
// Created by revol-xut on 11/2/19.
//

#include <boost/test/unit_test.hpp>
#include <string>
#include "../daemon/config/config.hpp"
#include "../daemon/config/network_conf.hpp"

#define CONFIG_FILE "../../tests/files/test_conf.json"

BOOST_AUTO_TEST_CASE(test_config_parsing) { //NOLINT
    /*
     * This unit tests checks if the reading and parsing function works.
     */

    internal_essential::Config config(CONFIG_FILE);

    BOOST_CHECK(config.getInteger("aa") == 1);
    BOOST_CHECK(config.getInteger("ab") == 2);
    BOOST_CHECK(config.getString("ac") == "Test");
    BOOST_CHECK(config.getBool("ad"));
}

BOOST_AUTO_TEST_CASE(test_config_default_normal) { //NOLINT
    /*
     * This unit test checks if all default values are loaded correctly
     */
    internal_essential::Config config;
    BOOST_CHECK(config.getString("routine_path") == "/etc/russel/routines/");
    BOOST_CHECK(config.getString("networkConf") == "/etc/russel/network.json");
}


BOOST_AUTO_TEST_CASE(test_config_default_network) { //NOLINT
    /*
     * Checks the default values from the network config_
     */
    internal_essential::NetworkConfig config;

    BOOST_CHECK(config.getString("host") == "127.0.0.1");
    BOOST_CHECK(config.getInteger("port") == 8321);
    BOOST_CHECK(config.getString("ipc") == "/run/russel.sock");
    BOOST_CHECK(config.getInteger("auto_connect_count") == 0);
    BOOST_CHECK(config.getBool("enable_ipc"));
    BOOST_CHECK(config.getBool("enable_pub"));
    BOOST_CHECK(config.getBool("enable_cli"));

}

BOOST_AUTO_TEST_CASE(test_config_casting_int) { //NOLINT
    /*
     * Checks if the cast to bool works fine
     */

    internal_essential::Config config(CONFIG_FILE);
    BOOST_CHECK(config.getInteger("aa") == 1);
    BOOST_CHECK(config.getInteger("ab") == 2);
}
