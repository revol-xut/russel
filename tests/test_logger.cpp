//
// Created by revol-xut on 11/2/19.
//

#include <boost/test/unit_test.hpp>
#include <fstream>
#include <string>

#include "../daemon/logging/logger.hpp"

#define LOGGING_FILE "../../tests/files/empty_logger.log"
#define DUMMY_TEXT "Lorem ipsum dolor sit amet, animal aperiri tacimates eos an."
#define DUMMY_TEXT_LENGTH 60

void discardFile() {
    /*
     * Clears the content of the test file
     */
    std::ofstream stream;
    stream.open(LOGGING_FILE, std::ios::out | std::ios::trunc);
    stream.close();
}


BOOST_AUTO_TEST_CASE(test_logger_write) { //NOLINT

    // Destroys all content of test logging file so the output is compare able
    discardFile();

    internal_essential::Logger logger(LOGGING_FILE, "INFO");

    auto lambda_check = [](int level, int threshold, internal_essential::Logger &logger, bool something,
                           const std::string &status,
                           int offSet) {
        discardFile();
        logger.setThreshold(threshold);
        logger.setStatus(level);

        std::string current_time;
        std::string expected;
        if (something) {
            // We do not compare the times so we leave them out
            expected = ": " + status + " : " + DUMMY_TEXT;
        }

        logger << DUMMY_TEXT;

        std::ifstream stream_file;
        stream_file.open(LOGGING_FILE);
        std::string temp_string;
        std::getline(stream_file, temp_string);

        if (!temp_string.empty()) {
            temp_string = temp_string.substr(temp_string.size() - DUMMY_TEXT_LENGTH - 9 - offSet,
                                             static_cast<unsigned long>(DUMMY_TEXT_LENGTH + 9 + offSet));
        }

        BOOST_CHECK(temp_string == expected);
    };

    lambda_check(1, 1, logger, true, "INFO", 0);
    lambda_check(1, 3, logger, false, "INFO", 0);
    lambda_check(4, 3, logger, true, "FATAL", 1);
    lambda_check(2, 3, logger, false, "DEBUG", 1);
    lambda_check(2, 1, logger, true, "DEBUG", 1);
}