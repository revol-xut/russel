//
// Created by revol-xut on 16.12.19.
//


#include <boost/test/unit_test.hpp>
#include <iostream>
#include "../daemon/addresser/murmur_hash.hpp"

#define DUMMY_TEXT "Lorem ipsum dolor sit amet, animal aperiri tacimates eos an."
#define DUMMY_TEXT_LENGTH 60
#define MURMUR_HASH 2271304579

BOOST_AUTO_TEST_CASE(test_murmurhash) { //NOLINT

    internal_essential::MurmurHash murmurHash{};
    murmurHash.Begin();
    murmurHash.Add(reinterpret_cast<const unsigned char *>(DUMMY_TEXT), DUMMY_TEXT_LENGTH);
    unsigned int hash = murmurHash.End();
    BOOST_CHECK(hash == MURMUR_HASH);

}