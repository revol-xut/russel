#!/bin/bash
# Automated Compilation of Dependencies of the Russel Project

eigen_repo='https://gitlab.com/libeigen/eigen.git'
russel_interpreter_repo='https://bitbucket.org/revol-xut/russel-interpreter/'
json_repo='https://github.com/nlohmann/json.git'

eigen_name='eigen'
russel_interpreter_name='russel-interpreter'
json_name='json'

current_repo=''
current_name=''

build_and_install(){
  echo "+****************** Building ... $2 ******************+"
  git clone $1
  mkdir -p $2/build && cd $2/build
  cmake ..
  make
  make install
  cd ../../
}

#Builds Eigen Library
build_and_install $eigen_repo $eigen_name

#Builds Nlohmann Json Library
build_and_install $json_repo $json_name

#Builds Russel Interpreter Library
build_and_install $russel_interpreter_repo $russel_interpreter_name


