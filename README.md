Worker - Russel 
------------------
![](https://img.shields.io/bitbucket/pipelines/revol-xut/russel?style=flat-square) 

Linux Daemon for distributing Tasks through a decentralised P2P network.

**Contact**: <revol-xut@protonmail.com>

## Compile
```bash

   $ mkdir build & cd build

   $ cmake .. -D_UNITTESTS=OFF

   $ make
```
**Note** that unittests wont be compiled.

## Install

Root permissions are required to put the service and config files into secure locations like /etc/

```bash

    $ sudo make install

```

## Run the Daemon

```bash

    $ ./daemon/russel

```

Systemd support will come soon.

## Dependencies

 * [Eigen3](http://eigen.tuxfamily.org) Linear Algebra and Math Stuff
 * [Nlohmann Json](https://github.com/nlohmann/json) Json Library for de/encoding
 * [Boost Test]( http://boost.org/libs/test) Only for unit tests
 * [Russel-Interpreter-Lib](https://bitbucket.org/revol-xut/russel-interpreter/) Byte Code interpreter

For a convenient setup there is the compile_dependencies.sh which automatically downloads the dependencies compiles and installs them.

## Change Log

**v0.2b**(2020.6.x) (Compatible with RI >= v0.1b)
 
 + Better Heart-Beat
 
**v0.2a**(2020.5.29) (Compatible with RI >= v0.1b)
+ Added Service files for systemd
+ Upgraded Threading techniques
+ Fixed Memeory Leaks

**v0.1f**(2020.5.25) (Compatible with RI >= v0.1b)

+ Added WorkScheduler, Task, Worker and TaskQueue classes
+ Added Base64 Encoding for raw Data
+ Daemon is now able to solve tasks locally
+ Added Tasks Sets
+ Fixed Network Issue

**v0.1e** (2020.4.8)

+ Added Class for system resource consumption
+ Added Classes for handling routines e.g. loading, saving etc.
+ Started Execution Manager 
+ Added Work Scheduler and Task Classes
+ Added Workload Estimator

**v0.1d** (2020.2.12)

+ Modernize Code
+ Internal Addressing System now using json
+ Information Commands
+ MurmurHash for generating ids
+ Introduced good way to close connections
+ API now includes endpoint ("api", "close" and "ping")

**v0.1c** (2019.12.12)

+ Added auto connect
+ Added basic network interface (e.g Heartbeat)
+ hardened listeners
+ Removed pre-allocated socket buffer
+ Improved cmake for testing purposes

**v0.1b** (2019.12.6)

+ Added AF_INET and AF_UNIX (ipc) Sockets
+ Added Socket Manager
+ Added proper installation
**Note**: The Daemon currently just sends back what he received


**v0.1a** (2019.11.22)

+ Added Logger, Config reader and handler
+ Added Runtime Object
