//
// Created by revol-xut on 18.02.20.
//

#ifndef RUSSEL_EXECUTION_HANDLER_HPP
#define RUSSEL_EXECUTION_HANDLER_HPP

#include <map>
#include <string>
#include <thread>
#include <random>
#include <numeric>
#include <russel-interpreter/runtime/runtime.hpp>

#include "../routines/routine_manager.hpp"
#include "../information_object.hpp"
#include "../work_scheduler/task.hpp"

constexpr unsigned int kExecutionHandler = 16;

class ExecutionHandler : public internal_essential::InformationObject {
public:

    ExecutionHandler();

    ~ExecutionHandler();

    /*!
     * @brief Creates new Russel Interpreter Runtime object
     * @param name name of the routine TODO: Maybe use integer instead (hash)
     * @return uuid of routine
     */
    auto stageRoutine(const std::string &name) -> unsigned int;

    /*!
     * @brief Generates init variables from given data
     * @param uuid uuid of the routine
     * @param data char pointer to the data field
     */
    void loadData(unsigned int id, const std::shared_ptr<char[]> &data,
                  unsigned int data_size);

    /*!
     * @brief Executes the stages routine
     * @param uuid of routine
     */
    void runRoutine(unsigned int id);

    /*!
     * @brief Stops running thread
     * @param id
     */
    void killRoutine(unsigned int id);

    /*!
     *
     *
     */
    void run_task(const std::shared_ptr<Task> &task);

    void addToRuntime() override;

    void autoRespond(const std::shared_ptr<json> &input_json, const std::shared_ptr<json> &output_json) override;

    auto hash() -> unsigned int override;

private:

    static auto generateID() -> unsigned int;

    auto getVariable(unsigned int id, unsigned int variable) -> datatypes::AbstractType &;

    unsigned int routine_count_, max_threads_;

    std::unique_ptr<RoutineManager> routine_manager_;

    std::map<unsigned int, std::shared_ptr<runtime::Runtime>> running_routines_ = {};
    std::map<unsigned int, std::shared_ptr<std::thread>> running_threads_ = {};
};


#endif //RUSSEL_EXECUTION_HANDLER_HPP
