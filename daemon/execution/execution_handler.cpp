//
// Created by revol-xut on 18.02.20.
//

#include "execution_handler.hpp"


ExecutionHandler::ExecutionHandler() {
    running_routines_ = {};
    running_threads_ = {};
    routine_count_ = 0;
    max_threads_ = runtime_->getConfig()->getInteger("max_threads");
    routine_manager_ = std::make_unique<RoutineManager>();
    addToRuntime();
}

ExecutionHandler::~ExecutionHandler() = default;

auto ExecutionHandler::stageRoutine(const std::string &name) -> unsigned int {

    auto routine_ptr = std::make_shared<Routine>();
    routine_manager_->load_routine(name, routine_ptr);

    if (routine_ptr != nullptr) {
        auto random_number = generateID();

        auto runtime = std::make_shared<runtime::Runtime>(routine_ptr->getRoutine());
        running_routines_[random_number] = runtime;

        routine_count_++;
        return random_number;
    }

    return 0;
}

void ExecutionHandler::loadData(unsigned int id,
                                const std::shared_ptr<char[]> &data,
                                unsigned int data_size) {
  if (running_routines_.find(id) != running_routines_.end()) {
    running_routines_.at(id)->setData(data, data_size);
  }  // TODO: parse variables from data otherwise we need memcpy
}

void ExecutionHandler::runRoutine(unsigned int id) {
    if (running_routines_.find(id) != running_routines_.end()) {

        //TODO: Technical place where scheduler and queue would come into play

        running_threads_[id] = std::make_unique<std::thread>(&runtime::Runtime::execute, (*running_routines_.at(id)));
    }
}

auto ExecutionHandler::generateID() -> unsigned int {
    std::default_random_engine generator;
    generator.seed(time(nullptr) * 1.45);
    std::uniform_int_distribution<unsigned int> distribution{};

    unsigned int random_number;
    do {
        random_number = distribution(generator);
    } while (random_number != 0); // O is reserved as error code

    return random_number;
}

auto ExecutionHandler::getVariable(unsigned int id, unsigned int variable) -> datatypes::AbstractType & {
    if (running_routines_.find(id) != running_routines_.end()) {
        return running_routines_.at(id)->getVariable(variable);
    } else {
        throw std::runtime_error("unhandled not found runtime id");
    }
}

void ExecutionHandler::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<ExecutionHandler>(this);
    }
}

auto ExecutionHandler::hash() -> unsigned int {
    return kExecutionHandler;
}

void ExecutionHandler::autoRespond(const std::shared_ptr<json> &input_json, const std::shared_ptr<json> &output_json) {
    std::string command = input_json->at("command");

    if (input_json->at("command") == "routine_count") {
        (*output_json)["routine_count"] = routine_count_;
    } else if (command == "stage_routine") {
        /*
         * Stages Routine and returns loaded routine id
         */
        std::string name = input_json->at("name");
        auto id = stageRoutine(name);

        if (id == 0) { // Some Issue
            (*output_json)["error"] = "did_not_found_routine";
        } else { // Success
            (*output_json)["id"] = id;
        }

    } else if (command == "load_data") {
        // Only for testing or small data sizes
        unsigned int id = input_json->at("id");
        std::vector<float> data = input_json->at("data");

        if (not data.empty()) {
            float *pointer_floats = &(data[0]);
            char *pointer_bytes = reinterpret_cast<char *>(pointer_floats);
            auto ptr = std::make_shared<char[]>(data.size() * sizeof(float));
            std::memcpy(ptr.get(), pointer_bytes, data.size() * sizeof(float)); //TODO: Fix
            this->loadData(id, ptr, data.size() * sizeof(float));
        }

    } else if (command == "execute") {

        unsigned int id = input_json->at("id");
        runRoutine(id);

    } else if (command == "get_variable") {
        unsigned int id = input_json->at("id");

        if (running_routines_.find(id) != running_routines_.end()) {
            unsigned int variable = input_json->at("variable");

            if (running_routines_.at(id)->getVariableCount() < variable) { // Variable does not exists
                (*output_json)["error"] = "no_variable_found";
                return;
            }

            datatypes::AbstractType &var = getVariable(id, variable); // its only a reference should not create a copy

            (*output_json)["data_type"] = var.getType();
            unsigned int size = var.getSize();

            auto data = std::make_shared<char[]>(size);
            var.serializeObject(data.get());

            (*output_json)["size"] = size;
            (*output_json)["data"] = std::string(data.get(), size); //TODO: pot bug when data deallocates

        } else {
            (*output_json)["error"] = "no_routine_found";
        }

    } else if (command == "running_count_and_max") {
        (*output_json)["running"] = routine_count_;
        (*output_json)["max"] = max_threads_;
    }
}