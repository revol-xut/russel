//
// Created by revol-xut on 16.12.19.
//

#ifndef RUSSEL_MURMUR_HASH_HPP
#define RUSSEL_MURMUR_HASH_HPP

#include <string>
#include <iostream>
#include <sstream>

namespace internal_essential {
    constexpr unsigned int kM = 0x5bd1e995;
    constexpr unsigned int kR = 24;

#define MMIX(h, k) { k *= kM; k ^= (k >> kR); k *= kM; h *= kM; h ^= k; }

    /*
    * @brief This class is hashes multiple data types (Implements Murmurhash 2)
    * Credit for original Implementation: https://sites.google.com/site/murmurhash/ (see MurmurHash2A)
    */

    class MurmurHash {
    public:

        void Begin(unsigned int seed = 0);

        void Add(const unsigned char *data, int len);

        auto End() -> unsigned int;

        static auto Int2Hex(unsigned int int_value) -> std::string;

        static auto Hex2Int(const std::string &hex_string) -> unsigned int;

    private:
        void MixTail(const unsigned char *&data, int &len);

        unsigned int m_hash_;
        unsigned int m_tail_;
        unsigned int m_count_;
        unsigned int m_size_;

    };

}


#endif //RUSSEL_MURMUR_HASH_HPP
