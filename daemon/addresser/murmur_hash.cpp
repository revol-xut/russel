//
// Created by revol-xut on 16.12.19.
//

#include "murmur_hash.hpp"

void internal_essential::MurmurHash::Begin(unsigned int seed) {
    m_hash_ = seed;
    m_tail_ = 0;
    m_count_ = 0;
    m_size_ = 0;
}

void internal_essential::MurmurHash::Add(const unsigned char *data, int len) {
    m_size_ += len;

    MixTail(data, len);

    while (len >= 4) {
        unsigned int k = *(unsigned int *) data;

        MMIX(m_hash_, k); //NOLINT its not an empty statement (pointer)

        data += 4;
        len -= 4;
    }

    MixTail(data, len);
}

auto internal_essential::MurmurHash::End() -> unsigned int {
    MMIX(m_hash_, m_tail_); //NOLINT its not an empty statement (pointer)
    MMIX(m_hash_, m_size_); //NOLINT its not an empty statement (pointer)

    m_hash_ ^= m_hash_ >> 13;
    m_hash_ *= kM;
    m_hash_ ^= m_hash_ >> 15;

    return m_hash_;
}

void internal_essential::MurmurHash::MixTail(const unsigned char *&data, int &len) {
    while (len && ((len < 4) || m_count_)) {
        m_tail_ |= (*data++) << (m_count_ * 8);

        m_count_++;
        len--;

        if (m_count_ == 4) {
            MMIX(m_hash_, m_tail_); //NOLINT its not an empty statement (pointer)
            m_tail_ = 0;
            m_count_ = 0;
        }
    }
}

auto internal_essential::MurmurHash::Int2Hex(unsigned int int_value) -> std::string {
    std::stringstream stream;
    stream << std::hex << int_value;
    return std::string(stream.str());
}

auto internal_essential::MurmurHash::Hex2Int(const std::string &hex_string) -> unsigned int {
    unsigned int int_value = 0;
    std::stringstream ss;
    ss << std::hex << hex_string;
    ss >> int_value;
    return int_value;
}