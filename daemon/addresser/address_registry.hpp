//
// Created by revol-xut on 16.12.19.
//

#ifndef RUSSEL_ADDRESS_REGISTRY_HPP
#define RUSSEL_ADDRESS_REGISTRY_HPP

#include <map>
#include <memory>
#include <nlohmann/json.hpp>

#include "../information_object.hpp"

namespace internal_essential {

    class AddressRegistry {
    public:
        AddressRegistry();

        ~AddressRegistry();

        auto resolve(unsigned int hash) -> nlohmann::json;

        void registerObject(const std::shared_ptr<internal_essential::InformationObject> &new_object);

    private:

        std::map<unsigned int, std::shared_ptr<internal_essential::InformationObject>> hash_table_;

    };
}


#endif //RUSSEL_ADDRESS_REGISTRY_HPP
