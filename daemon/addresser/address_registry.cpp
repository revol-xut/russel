//
// Created by revol-xut on 16.12.19.
//

#include "address_registry.hpp"

internal_essential::AddressRegistry::AddressRegistry() = default;

internal_essential::AddressRegistry::~AddressRegistry() = default;

auto internal_essential::AddressRegistry::resolve(unsigned int hash) -> nlohmann::json {
    if (hash_table_.find(hash) != hash_table_.end()) {

        json data, output;
        hash_table_.at(hash)->autoRespond(std::make_shared<json>(data),
                                          std::make_shared<json>(output));

        return output;
    } else {
        return nlohmann::json::array({{"error", "unknown hash"}});
    }
}

void
internal_essential::AddressRegistry::registerObject(
        const std::shared_ptr<internal_essential::InformationObject> &new_object) {
    hash_table_[new_object->hash()] = new_object;
}