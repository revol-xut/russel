//
// Created by revol-xut on 11/1/19.
//

/*
 * This is the file which contains all signal handlers which log the signal
 */
#include <csignal>
#include <string>

#include "../global.hpp"

void signalHandler(int sig) {

    if (kRuntime == nullptr) {
        std::cerr << "No runtime object is running pls report this bug with your configuration" << std::endl;
        exit(1);
    }

    std::string err;

    switch (sig) {
        case SIGHUP:
            // Signal hang up signals a too reload configuration
            kRuntime->getConfig()->reload();
            kRuntime->getNetworkConfig()->reload();
            break;

        case SIGTERM:
            // Termination Request
            kRuntime->terminate();
            break;
        default:
            break;
    }

    err = strsignal(sig);

    kRuntime->getLogger(DEBUG) << "Caught Signal: " + err;

}

