//
// Created by revol-xut on 11/1/19.
//

#include "logger.hpp"

#include <iostream>

internal_essential::Logger::Logger(const std::string &file, const std::string &status) {
  this->path_ = file;
  this->status_ = status;
  this->use_sysLog_ = true;
  this->logging_threshold_ = 0;  // TODO: set this via config
  this->logging_level_ = 1;
}

internal_essential::Logger::Logger() {
    this->status_ = "INFO";
    this->path_ = "";
    this->use_sysLog_ = true;
    this->logging_threshold_ = 0;
    this->logging_level_ = 1;
}

internal_essential::Logger::~Logger() {
    log_file_.close();
}

void internal_essential::Logger::operator<<(const std::string &message) {
    // Maybe Change is you know how to write the stream buffer
    if (logging_level_ >= logging_threshold_) {
        setLoggingString();
        log_file_.open(path_, std::ios::app);
        printTime();
        log_file_ << ": " << status_ << " : " << message << "\n";
        log_file_.close();
    }
}

void internal_essential::Logger::printTime() {
    constexpr int year_unix = 1900;

    if (log_file_.is_open()) {
        time_t now = time(nullptr);

        tm *ltm = localtime(&now);

        log_file_ << ltm->tm_mday << ".";
        log_file_ << 1 + ltm->tm_mon << ".";
        log_file_ << year_unix + ltm->tm_year << " ";
        log_file_ << 1 + ltm->tm_hour << ":";
        log_file_ << 1 + ltm->tm_min << ":";
        log_file_ << 1 + ltm->tm_sec << " ";
    }
}

void internal_essential::Logger::setLoggingString() {
    switch (logging_level_) {
        case 1:
            status_ = "INFO";
            return;
        case 2:
            status_ = "DEBUG";
            return;
        case 3:
            status_ = "ERROR";
            return;
        case 4:
            status_ = "FATAL";
            return;
        default:
            status_ = "DEBUG";
            return;
    }
}