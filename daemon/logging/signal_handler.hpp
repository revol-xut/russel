//
// Created by revol-xut on 11/2/19.
//

#ifndef WORKER_SIGNAL_HANDLER_H
#define WORKER_SIGNAL_HANDLER_H

/*!
 * @brief Handles Unix Signals
 * @param sig Corresponding Signal
 */
void signalHandler(int sig);

#endif //WORKER_SIGNAL_HANDLER_H
