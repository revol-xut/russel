//
// Created by worker on 12/6/19.
//

#include "network_interface.hpp"

#include "../information_object.hpp"

using json = nlohmann::json;

std::vector<std::string> NetworkInterface::temporary_ = {};

NetworkInterface::NetworkInterface() { temporary_ = {}; }

NetworkInterface::~NetworkInterface() = default;

void NetworkInterface::executeStack(
    const std::shared_ptr<MessageList> &messages,
    const std::shared_ptr<MessageList> &response) {
  temporary_.clear();  // Deallocation of old messages
  bool create_new = true;
  std::shared_ptr<RawMessage> response_message;

  for (const std::shared_ptr<RawMessage> &msg : messages->messages) {
    if (msg->size > 0 and msg->message != nullptr) {
      if (create_new) {
        response_message = std::make_shared<RawMessage>();
      }
      try {
        create_new = execute(msg, response_message);
      } catch (const std::exception &e) {
        std::string error_message =
            std::string("Error Occurred: ") + std::string(e.what()) +
            " with message: " + std::string(msg->message, msg->size);
        std::cerr << error_message << std::endl;
        getRuntime()->getLogger(ERROR) << error_message;
        getRuntime()->getLogger(DEBUG) << std::string(msg->message, msg->size);
        create_new = false;  // TODO: maybe return 500 Internal server error
      }
      if (create_new) {
        response->messages.push_back(response_message);
        response->message_count++;
      }
    }
  }
}
auto NetworkInterface::execute(const std::shared_ptr<RawMessage> &message,
                               const std::shared_ptr<RawMessage> &response)
    -> bool {
  response->socket = message->socket;
  response->origin = message->origin;

  auto data = json::parse(std::string(message->message, message->size));

  if (data.find("command") == data.end()) {
    response->message = "{'error':'missing_key_command'}";
    response->size = sizeof("{'error':'missing_key_command'}");
    return true;
  }

  std::string command = data.at("command");

  // ################################ Ping ################################
  if (command == "ping") {
    response->message = "{'data':'heart-beat-return'}";
    response->size = sizeof("{'data':'heart-beat-return'}");

    // ################################ API ################################
  } else if (command == "api") {
    if (data.find("id") == data.end()) {
      response->message = "{'error':'missing_key_id'}";
      response->size = sizeof("{'error':'missing_key_id'}");
      return true;
    }

    int id = data.at("id");

    // https://github.com/nlohmann/json_docs/blob/master/docs/exceptions.md
    if (data.find("data") == data.end()) {
      response->message = "{'error':'missing_key_data'}";
      response->size = sizeof("{'error':'missing_key_data'}");
      return true;
    }

    if (!data.at("data").is_object()) {
      response->message = "{'error':'data_should_be_json'}";
      response->size = sizeof("{'error':'data_should_be_json'}");
      return true;
    }

    json raw_data = data.at("data");
    raw_data["socket_id"] = message->socket;
    raw_data["socket_origin"] = message->origin;

    auto data_container = std::make_shared<json>(raw_data);
    auto return_value = std::make_shared<json>();

    if (data.find("token") != data.end()) {
      (*return_value)["token"] = data.at("token");
      (*data_container)["token"] = data.at("token");
    }

    objects_.at(id)->autoRespond(data_container, return_value);

    std::string string_message = (*return_value).dump();
    delete[] response->message;

    response->message = new char[string_message.size()];

    std::memcpy((void *)response->message, string_message.c_str(),
                string_message.size());  // TODO: optimize dont memcpy that shit
    response->size = string_message.size();
    return true;

    // ################################ Close ################################
  } else if (command == "close") {
    if (objects_.find(message->origin + 1) == objects_.end()) {
      runtime_->getLogger(FATAL)
          << "Can not find object with id: " + std::to_string(message->origin) +
                 " while trying to close a socket.";
      response->message = "{'error':'internal_error_could_not_close_socket'}";
      response->size =
          sizeof("{'error':'internal_error_could_not_close_socket'}");
      return true;
    }
    auto data_container = std::make_shared<json>();
    auto return_value = std::make_shared<json>();
    (*data_container)["command"] = "delete_socket";
    (*data_container)["data"]["socket"] = message->socket;

    objects_.at(message->origin + 1)->autoRespond(data_container, return_value);

    char string[] =
        R"({"command":"api", "id":22, "data":{"command":"unregister"}})";
    auto new_message = std::make_shared<RawMessage>();
    new_message->message = string;
    new_message->size = sizeof(string);
    new_message->socket = message->socket;
    new_message->origin = message->origin;

    execute(new_message, message);
    return false;  // Does not need to create new message object

    // ################################ Workload ##############################
  } else if (command == "workload") {
    char string[] =
        R"({"command":"api", "id":21, "data":{"command":"system_resource"}})";
    auto new_message = std::make_shared<RawMessage>();
    new_message->message = reinterpret_cast<const char *>(&string);
    new_message->size = sizeof(string);
    new_message->socket = message->socket;
    new_message->origin = message->origin;

    return execute(new_message, message);

    // ################################ Register_Workload #####################
  } else if (command == "register_workload") {
    auto input_json = std::make_shared<nlohmann::json>();
    auto output_json = std::make_shared<nlohmann::json>();

    (*input_json)["command"] = "register_workload";
    (*input_json)["workload"] = data.at("workload");
    (*input_json)["socket_id"] = message->socket;
    (*input_json)["socket_origin"] = message->origin;

    objects_.at(22)->autoRespond(input_json, output_json);

    // ################################ Task_Return ###########################
  } else if (command == "task_return") {
    auto input_json = std::make_shared<nlohmann::json>();
    auto output_json = std::make_shared<nlohmann::json>();
    std::cout << "task return " << std::endl;
    (*input_json)["command"] = "get_information_and_delete";
    (*input_json)["token"] = data.at("token");

    objects_.at(24)->autoRespond(input_json, output_json);

    if (output_json->at("found")) {
      response->socket = output_json->at("socket");
      response->origin = output_json->at("origin");

      response->message = message->message;
    }

  } else {
    response->message = "{'error':'unknown-command'}";
    response->size = sizeof("{'error':'unknown-command'}");
  }
  return true;
}

void NetworkInterface::addToRuntime() {}

auto NetworkInterface::hash() -> unsigned int { return kNetworkInterfaceID; }

void NetworkInterface::autoRespond(const std::shared_ptr<json> &input_json,
                                   const std::shared_ptr<json> &output_json) {
  std::string command = input_json->at("command");

  if (command == "check_online") {
    unsigned int id = input_json->at("id");
    (*output_json)["online"] = (objects_.find(id) != objects_.end());
  }
}