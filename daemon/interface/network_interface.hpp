//
// Created by revol-xut on 12/6/19.
//

#ifndef RUSSEL_NETWORK_INTERFACE_HPP
#define RUSSEL_NETWORK_INTERFACE_HPP

#include <memory.h>

#include <list>

#include "../information_object.hpp"
#include "../network/socket.hpp"
#include "../network/socket_handler.hpp"

constexpr int kTimeOut = 250;
constexpr unsigned int kNetworkInterfaceID = 7;

using json = nlohmann::json;

class NetworkInterface : public internal_essential::InformationObject {
public:
    NetworkInterface();

    ~NetworkInterface();

    /*!
     * @brief Executes a entire stack of messages.
     * @param messages Request message stack
     * @param response response messages
     */
    static void
    executeStack(const std::shared_ptr<MessageList> &messages, const std::shared_ptr<MessageList> &response);

    /*!
     * @brief Responses too a given Message.
     * @param message Message
     * @param response  Response Message
     * @return system is responding -> when false response is empty
     */
    static auto execute(const std::shared_ptr<RawMessage> &data,
                        const std::shared_ptr<RawMessage> &response) -> bool;

    //   static auto
    //  execute_api(const std::shared_ptr<nlohmann::json> &data, unsigned int
    //  id, int socket, unsigned short origin,
    //              const std::shared_ptr<RawMessage> &response) -> bool;

    void addToRuntime() override;

    void autoRespond(const std::shared_ptr<json> &input_json,
                     const std::shared_ptr<json> &output_json) override;

    auto hash() -> unsigned int override;

    static std::vector<std::string> temporary_;  // TODO: change to list

   private:
    // void correct_corrupted_message(const std::shared_ptr<RawMessage>& ptr);

    // static auto stage_execution(const std::shared_ptr<RawMessage> &message,
    // const std::shared_ptr<RawMessage> &response) -> bool;
};


#endif //RUSSEL_NETWORK_INTERFACE_HPP
