//
// Created by einspaten on 10/06/2020.
//

#ifndef RUSSEL_TASK_FORWARDER_HPP
#define RUSSEL_TASK_FORWARDER_HPP

#include "../information_object.hpp"

struct TaskReturn {
  int socket;
  unsigned short origin;
  std::string uuid;
};

constexpr unsigned int kTaskForwarder = 24;

class TaskForwarder : public internal_essential::InformationObject {
 public:
  TaskForwarder();
  ~TaskForwarder();

  void autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                   const std::shared_ptr<nlohmann::json> &output_json) override;

  auto hash() -> unsigned int override;

  void addToRuntime() override;

 private:
  std::map<std::string, std::shared_ptr<TaskReturn>> waiting_tasks_;
};

#endif  // RUSSEL_TASK_FORWARDER_HPP
