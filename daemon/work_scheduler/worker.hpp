//
// Created by einspaten on 11.04.20.
//

#ifndef RUSSEL_WORKER_HPP
#define RUSSEL_WORKER_HPP

#include <chrono>
#include <condition_variable>
#include <mutex>
#include <thread>

#include "../network/socket.hpp"
#include "task_queue.hpp"

class Worker {
 public:
  /*!
   * @brief Starts loop function in separate thread
   */
  virtual void wake_up() = 0;
  auto get_network_speed_rating() -> unsigned int;
  auto get_computational_power_rating() -> unsigned int;
  auto get_queue() -> std::shared_ptr<TaskQueue>;
  auto get_identity() -> bool;

 protected:
  auto new_stuff() -> bool;

  bool running_ = true;
  bool new_stuff_ = false;
  bool identity_ = false;  // False => Local Worker True => Network Worker

  unsigned int network_speed_rating_ = 0;
  unsigned int computational_power_rating_ = 0;

  std::shared_ptr<TaskQueue> queue_;

  std::mutex mutex_;
  std::condition_variable cv_;
};

class LocalWorker : public Worker {
 public:
  LocalWorker();
  ~LocalWorker();

  void wake_up() override;

 private:
  void loop();

  std::unique_ptr<std::thread> thread_;
  unsigned int thread_id = 0;  // TODO: remove
};

class NetworkWorker : public Worker {
 public:
  NetworkWorker();
  NetworkWorker(int socket, unsigned short origin);

  ~NetworkWorker();

  void wake_up() override;

  auto equivalent(int socket, unsigned short origin) const -> bool;

 private:
  int socket_ = -1;
  unsigned short origin_ = 0;

  std::vector<std::shared_ptr<Task>> pending_tasks_for_return_;
};

#endif //RUSSEL_WORKER_HPP
