//
// Created by einspaten on 06.04.20.
//

#include "scheduler_engine.hpp"

#include <experimental/filesystem>
#include <functional>
#include <random>

#include "../other/base64.hpp"
#include "hungarian_algo.hpp"

SchedulerEngine::SchedulerEngine() {
  tasks_ = {};
  available_workers_ = {};
  task_sets_ = {};
  batch_size_ = 0;
  last_schedule_ = std::time(nullptr);
  update_timer_ = runtime_->getConfig()->getInteger("scheduler_update");
  thread_workers();
  addToRuntime();
}

SchedulerEngine::~SchedulerEngine() = default;

void SchedulerEngine::autoRespond(
    const std::shared_ptr<nlohmann::json> &input_json,
    const std::shared_ptr<nlohmann::json> &output_json) {
  std::string command = input_json->at("command");
  if (command == "batch_size") {
    (*output_json)["size"] = tasks_.size();
  } else if (command == "workers_count") {
    (*output_json)["worker_count"] = available_workers_.size();
  } else if (command == "register_new_task") {
    std::string routine_name = input_json->at("routine_name");
    std::string data = input_json->at("data");
    std::string decoded;

    std::cout << "received task" << std::endl; //TODO: remove

    if (not std::experimental::filesystem::exists(
            runtime_->getConfig()->getString("routine_path") + routine_name)) {
      (*output_json)["error"] = "routine_does_not_exists";
      return;
    }

    Base64::decode(data, decoded);
    std::vector<unsigned int> required_vars = input_json->at("required_vars");

    auto new_task = std::make_shared<Task>(routine_name, decoded);

    new_task->set_required_vars(required_vars);
    new_task->set_network_stuff(input_json->at("socket_id"),
                                input_json->at("socket_origin"));

    new_task->set_task_token(input_json->at("token"));
    // new_task->random_task_id();
    tasks_.push_back(new_task);

    if (tasks_.size() >= available_workers_.size() or
        std::time(nullptr) - last_schedule_ > update_timer_) {
      schedule();
    }

    (*output_json)["command"] = "task_added";
  } else if (command == "force_schedule") {
    if (tasks_.empty()) {
      (*output_json)["error"] = "there_are_no_task_to_schedule";
      return;
    } else {
      schedule();
      (*output_json)["success"] = true;
    }
  } else if (command == "add_network_worker") {
    unsigned short origin = input_json->at("socket_origin");
    int socket = input_json->at("socket_id");

    auto worker = std::make_shared<NetworkWorker>(socket, origin);

    available_workers_.push_back(worker);

    (*output_json)["success"] = true;
  } else if (command == "remove_network_worker") {
    unsigned short origin = input_json->at("socket_origin");
    int socket = input_json->at("socket_id");
    unsigned i = 0;
    int index = -1;

    for (const auto &worker : available_workers_) {
      if (worker->get_identity()) {
        if (((NetworkWorker *)(void *)worker.get())  // NOLINT
                ->equivalent(socket, origin)) {
          index = i;
        }
      }
      i++;
    }

    if (index > 0) {
      available_workers_.erase(available_workers_.begin() + index);
    }
  } else if (command == "create_task_set") {
    std::string routine_name = input_json->at("routine_name");
    std::string data = input_json->at("data");
    std::vector<unsigned int> unset_vars = input_json->at("unset_variables");
    std::string decoded;
    if (not std::experimental::filesystem::exists(
            runtime_->getConfig()->getString("routine_path") + routine_name)) {
      (*output_json)["error"] = "routine_does_not_exists";
      (*output_json)["diagnostics"] =
          runtime_->getConfig()->getString("routine_path") + routine_name;
      return;
    }

    Base64::decode(data, decoded);
    std::vector<unsigned int> required_vars = input_json->at("required_vars");

    auto task_set = std::make_shared<TaskSet>(
        runtime_->getConfig()->getString("routine_path") + routine_name,
        decoded, unset_vars);
    task_set->set_required_vars(required_vars);

    auto id = random_number();

    task_sets_[id] = task_set;
    (*output_json)["task_set_id"] = id;

  } else if (command == "remove_task_set") {
    unsigned int task_set_id = input_json->at("task_set_id");
    std::cout << "deleting task set: " << task_set_id << std::endl;
    task_sets_.erase(task_set_id);
    (*output_json)["success"] = true;
  } else if (command == "task_from_set") {
    unsigned int task_set_id = input_json->at("task_set_id");
    std::string data = input_json->at("data");
    std::string decoded;

    if (task_sets_.find(task_set_id) == task_sets_.end()) {
      (*output_json)["error"] = "task_set_does_not_exists";
      return;
    }

    Base64::decode(data, decoded);
    auto task = task_sets_.at(task_set_id)->set_data(decoded);

    task->set_network_stuff(input_json->at("socket_id"),
                            input_json->at("socket_origin"));
    task->set_task_token(input_json->at("token"));

    tasks_.push_back(task);
    if (tasks_.size() >= available_workers_.size() or
        std::time(nullptr) - last_schedule_ > update_timer_) {
      schedule();
    }
  } else if (command == "remove_dead_task_sets") {
    unsigned int delete_index = 0;
    unsigned int iterator = 0;

    for (const std::pair<const unsigned int, std::shared_ptr<TaskSet>>
             &task_set_pair : task_sets_) {
      if (task_set_pair.second->get_time_of_last_update() >
          kDeleteTaskSetTimer) {
        delete_index = iterator;
        std::cout << "garbage collecting task set" << std::endl;
        break;
      }
      iterator++;
    }
    task_sets_.erase(delete_index);
  }
}

void SchedulerEngine::schedule() {
  // Following Steps are performed
  // Create weight matrix
  // Than run Hungarian Algorithm
  // Than push tasks into the queues from the various workers

  if (available_workers_.empty() or tasks_.empty()) {
    return;
  }

  unsigned int row = 0;
  unsigned int column = 0;
  unsigned int weight;

  std::vector<std::vector<double>> weight_matrix = {};
  std::vector<int> assignment = {};

  for (row = 0; row < tasks_.size(); row++) {
    weight_matrix.emplace_back();

    for (column = 0; column < available_workers_.size();
         column++) {  // TODO: Usage of systems is not included
      weight =
          (available_workers_.at(column)->get_network_speed_rating() *
           tasks_.at(row)->get_transmission_size()) +
          (available_workers_.at(column)->get_computational_power_rating() *
           tasks_.at(row)->get_estimated_workload());
      weight_matrix[row].push_back(weight);
    }
  }

  auto hungarian_algorithm = std::make_unique<HungarianAlgorithm>();
  hungarian_algorithm->Solve(weight_matrix, assignment);

  for (unsigned int worker = 0; worker < assignment.size(); worker++) {
    available_workers_.at(worker)->get_queue()->push_back(
        tasks_.at(assignment.at(worker)));
  }
  tasks_.erase(
      tasks_.begin(),
      tasks_.begin() + std::min(available_workers_.size(), tasks_.size()));

  last_schedule_ = std::time(nullptr);
  batch_size_ = tasks_.size();
}

auto SchedulerEngine::hash() -> unsigned int { return kScheduleEngineID; }

void SchedulerEngine::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<SchedulerEngine>(this);
  }
}

void SchedulerEngine::thread_workers() {
  unsigned int thread_workers =
      runtime_->getConfig()->getInteger("thread_workers");
  std::shared_ptr<Worker> temp_worker;

  for (unsigned int i = 0; i < thread_workers; i++) {
    temp_worker = std::make_shared<LocalWorker>();
    available_workers_.push_back(temp_worker);
  }
}

auto SchedulerEngine::random_number() -> unsigned int {
  srand(static_cast<unsigned int>(time(NULL)));

  return random();
}