//
// Created by einspaten on 29.03.20.
//

#include "other_engine_controller.hpp"

OtherEngineController::OtherEngineController() {
  network_entities_ = {};
  last_update_ = std::time(nullptr);
  addToRuntime();
}

OtherEngineController::~OtherEngineController() = default;

void OtherEngineController::autoRespond(
    const std::shared_ptr<nlohmann::json> &input_json,
    const std::shared_ptr<nlohmann::json> &output_json) {
  std::string command = input_json->at("command");

  if (command == "register") {
    auto tuple = std::make_tuple(input_json->at("socket_id"),
                                 input_json->at("socket_origin"));

    auto temp = std::make_shared<NetworkEntityInformation>();
    temp->last_update = 0;
    temp->is_russel_engine = false;
    network_entities_[tuple] = temp;

    if (input_json->find("is_engine") != input_json->end() and
        input_json->at("is_engine")) {
      temp->is_russel_engine = true;
      (*input_json)["command"] = "add_network_worker";
      objects_.at(23)->autoRespond(input_json, output_json);
    } else {
      temp->is_russel_engine = false;
    }
    std::cout << input_json->dump() << std::endl;
    std::cout << "New Engine: " + std::to_string(temp->is_russel_engine) << "/"
              << (input_json->find("is_engine") != input_json->end())
              << std::endl;
    if (runtime_ != nullptr) {
      runtime_->getLogger(INFO)
          << "New Engine: " + std::to_string(temp->is_russel_engine);
    }

    if (input_json->find("respond") != input_json->end() and
        input_json->at("respond")) {
      auto temp_input_json = std::shared_ptr<nlohmann::json>();
      (*temp_input_json)["command"] = "api";
      (*temp_input_json)["id"] = 22;
      (*temp_input_json)["data"]["command"] = "register";
      (*temp_input_json)["data"]["is_engine"] = true;
      auto temp_output_json = std::shared_ptr<nlohmann::json>();

      objects_.at(0)->autoRespond(temp_input_json, temp_output_json);
    }
  } else if (command == "unregister") {
    auto tuple = std::make_tuple(input_json->at("socket_id"),
                                 input_json->at("socket_origin"));

    if (network_entities_.find(tuple) == network_entities_.end()) {
      (*output_json)["error"] = "there_is_no_connection_registered_as_engine";
    } else {
      network_entities_.erase(tuple);

      (*input_json)["command"] = "remove_network_worker";
      objects_.at(23)->autoRespond(input_json, output_json);
    }
  } else if (command == "set_workload") {
    auto tuple = std::make_tuple(input_json->at("socket_id"),
                                 input_json->at("socket_origin"));

    if (input_json->find("workload") == input_json->end()) {
      (*output_json)["error"] = "no_key_called_workload";
      return;
    }

    auto data = network_entities_.at(tuple);
    data->last_update = std::time(nullptr);
    data->work_load_estimate = (*input_json)["workload"];
    data->is_russel_engine = true;

    (*output_json)["success"] = true;
  } else if (command == "update") {
    if (requires_update()) {
      send_heart_beat();
    }
  } else if (command == "register_worker") {
    auto tuple = std::make_tuple(input_json->at("socket_id"),
                                 input_json->at("socket_origin"));
    network_entities_.at(tuple)->is_russel_engine = true;
  } else if (command == "register_workload") {
    auto tuple = std::make_tuple(input_json->at("socket_id"),
                                 input_json->at("socket_origin"));

    if (network_entities_.find(tuple) != network_entities_.end()) {
      network_entities_.at(tuple)->work_load_estimate =
          input_json->at("workload");
      network_entities_.at(tuple)->last_update =
          static_cast<unsigned int>(std::time(nullptr));
    }
  }
}

auto OtherEngineController::hash() -> unsigned int {
  return kOtherEngineControllerId;
}

void OtherEngineController::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<OtherEngineController>(this);
  }
}

void OtherEngineController::send_heart_beat() {
  std::map<std::tuple<int, unsigned int>,
           std::shared_ptr<NetworkEntityInformation>>::iterator it;

  for (it = network_entities_.begin(); it != network_entities_.end(); it++) {
    if (it->second->is_russel_engine) {
      auto input_json = std::make_shared<nlohmann::json>();

      nlohmann::json data_json = {
          {"command", "register_workload"},
          {"workload", SystemResource::getWorkloadRating()}};

      (*input_json)["command"] = "send_message";
      (*input_json)["socket"] = std::get<0>(it->first);
      (*input_json)["origin"] = std::get<1>(it->first);
      (*input_json)["data"] = data_json.dump();

      auto temp_output_json = std::shared_ptr<nlohmann::json>();

      objects_.at(0)->autoRespond(input_json, temp_output_json);
    }
  }

  last_update_ = std::time(nullptr);
}

auto OtherEngineController::requires_update() -> bool {
  return std::time(nullptr) - last_update_ > kMinUpdateTimer;
}