//
// Created by einspaten on 10/06/2020.
//

#include "task_forwarder.hpp"

TaskForwarder::TaskForwarder() {
  waiting_tasks_ = {};
  addToRuntime();
}

TaskForwarder::~TaskForwarder() = default;

auto TaskForwarder::hash() -> unsigned int { return kTaskForwarder; }

void TaskForwarder::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<TaskForwarder>(this);
  }
}

void TaskForwarder::autoRespond(
    const std::shared_ptr<nlohmann::json> &input_json,
    const std::shared_ptr<nlohmann::json> &output_json) {
  std::string command = input_json->at("command");

  if (command == "add_waiting_task") {
    auto data = std::make_shared<TaskReturn>();
    data->uuid = input_json->at("token");
    data->socket = input_json->at("socket");
    data->origin = input_json->at("origin");

    waiting_tasks_[data->uuid] = data;
  } else if (command == "get_information_and_delete") {
    std::string token = input_json->at("token");
    bool found_object = false;

    for (const std::pair<std::string, std::shared_ptr<TaskReturn>> task_return : waiting_tasks_) {
      if (task_return.first == token) {
        found_object = true;
        (*output_json)["socket"] = task_return.second->socket;
        (*output_json)["origin"] = task_return.second->origin;
        break;
      }
    }

    if (found_object) {
      waiting_tasks_.erase(token);
    }

    (*output_json)["found"] = found_object;
  }
}