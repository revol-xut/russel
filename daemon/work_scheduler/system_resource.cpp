//
// Created by revol-xut on 13.02.20.
//

#include <iostream>
#include <cmath>
#include <algorithm>

#include "system_resource.hpp"


static unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;
static struct sysinfo memInfo;

SystemResource::SystemResource() {
    SystemResource::init();
    addToRuntime(); //NOLINT
    lastTotalSys = 0;
    lastTotalUserLow = 0;
    lastTotalUser = 0;
    lastTotalIdle = 0;
}

SystemResource::~SystemResource() = default;


auto SystemResource::getRamConsumption() -> double {
    auto virtualMemUsed = memInfo.totalram - memInfo.freeram;
    virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
    virtualMemUsed *= memInfo.mem_unit;

    long long totalPhysMem = memInfo.totalram;
    //Multiply in next statement to avoid int overflow on right hand side...
    totalPhysMem *= memInfo.mem_unit;

    double return_value = virtualMemUsed / std::max(totalPhysMem, static_cast<long long int>(1));

    return return_value;
}

auto SystemResource::getCpuConsumption() -> double {
    // Credit: https://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
    double percent;

    FILE *file;
    unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;
    file = fopen("/proc/stat", "r");

    fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
           &totalSys, &totalIdle);
    fclose(file);


    if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
        totalSys < lastTotalSys || totalIdle < lastTotalIdle) {
        //Overflow detection. Just skip this value.
        percent = -1.0;
    } else {
        total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
                (totalSys - lastTotalSys);
        percent = total;
        total += (totalIdle - lastTotalIdle);
        percent /= total;
        percent *= 100;
    }

    lastTotalUser = totalUser;
    lastTotalUserLow = totalUserLow;
    lastTotalSys = totalSys;
    lastTotalIdle = totalIdle;

    return percent;
}

auto SystemResource::getWorkloadRating() -> unsigned int {
  double usage = (getRamConsumption() + getCpuConsumption()) * 100;
  return std::round(usage);
}

void SystemResource::init() {
  FILE *file = fopen("/proc/stat", "r");
  fscanf(file, "cpu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow,
         &lastTotalSys, &lastTotalIdle);
  fclose(file);
}

void SystemResource::autoRespond(const std::shared_ptr<json> &input_json,
                                 const std::shared_ptr<json> &output_json) {
  std::string command = input_json->at("command");

    if (command == "cpu") {
        (*output_json)["cpu_usage"] = getCpuConsumption();
    } else if (command == "ram") {
        (*output_json)["ram_usage"] = getRamConsumption();
    } else if (command == "system_resource") {
        double usage = (getRamConsumption() + getCpuConsumption()) * 100;
        (*output_json)["system_usage"] = std::round(usage);
    }
}

auto SystemResource::hash() -> unsigned int {
    return kSystemResourceId;
}

void SystemResource::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<SystemResource>(this);
    }
}