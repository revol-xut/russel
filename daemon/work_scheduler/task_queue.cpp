//
// Created by einspaten on 11.04.20.
//

#include "task_queue.hpp"

std::vector<std::shared_ptr<TaskQueue>> TaskQueue::all_task_queues_ = {};

TaskQueue::TaskQueue() {
  worker_ = nullptr;
  tasks_ = {};
}

TaskQueue::TaskQueue(const std::shared_ptr<Worker> &worker) {
  worker_ = worker;
  tasks_ = {};
}

TaskQueue::~TaskQueue() = default;

void TaskQueue::push_back(const std::shared_ptr<Task> &task) {
  tasks_.push_back(task);

  if (worker_ != nullptr) {
    worker_->wake_up();
  }
}

auto TaskQueue::get_task() -> std::shared_ptr<Task> {
  // Checks if the own task_queue still has tasks
  if (not tasks_.empty()) {
    std::shared_ptr<Task> task = tasks_.at(0);
    tasks_.erase(tasks_.begin());
    return task;
  }

  // task queue of this worker is empty to the work balancer is triggert
  // Searches Work Queue with highest length
  unsigned int current_size = std::numeric_limits<unsigned int>::min();
  std::shared_ptr<TaskQueue> task_queue_max_size = nullptr;
  for (const std::shared_ptr<TaskQueue> &task_queue : all_task_queues_) {
    if (task_queue->size() > current_size) {
      task_queue_max_size = task_queue;
    }
  }
  std::shared_ptr<Task> task = nullptr;

  // Pulls Task that has the highest synergy
  if (task_queue_max_size != nullptr) {
    task = task_queue_max_size->get_highest_synergy(
        worker_->get_computational_power_rating(),
        worker_->get_network_speed_rating());
  }

  return task;
}

auto TaskQueue::get_highest_synergy(unsigned int comp_power,
                                    unsigned int network_speed)
    -> std::shared_ptr<Task> {
  unsigned int weight = 0;
  unsigned int temp_weight = 0;
  unsigned int index = 0;
  unsigned int i = 0;

  if (empty()) {
    return nullptr;
  }

  for (const std::shared_ptr<Task> &task : tasks_) {
    temp_weight = (network_speed * task->get_transmission_size()) +
                  (comp_power * task->get_estimated_workload());

    if (temp_weight > weight) {
      weight = temp_weight;
      index = i;
    }
    i++;
  }
  std::shared_ptr<Task> task = tasks_.at(index);
  tasks_.erase(tasks_.begin() + index);
  return task;
}

auto TaskQueue::empty() -> bool { return tasks_.begin() == tasks_.end(); }

auto TaskQueue::size() -> unsigned int { return tasks_.size(); }