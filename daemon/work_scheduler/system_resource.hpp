//
// Created by revol-xut on 13.02.20.
//

#ifndef RUSSEL_SYSTEM_RESOURCE_HPP
#define RUSSEL_SYSTEM_RESOURCE_HPP

#include <sys/sysinfo.h>
#include <sys/types.h>

#include "../information_object.hpp"

using json = nlohmann::json;

constexpr unsigned int kSystemResourceId = 21;

class SystemResource : public internal_essential::InformationObject {
public:

    SystemResource();

    ~SystemResource();

    /*!
     * @brief Returns absolute virtual ram consumption
     * @return ram consumption
     */
    static auto getRamConsumption() -> double;

    /*!
     * @brief Returns cpu usage in percent
     * @return cpu usage
     */
    static auto getCpuConsumption() -> double;

    static auto getWorkloadRating() -> unsigned int;

    void autoRespond(const std::shared_ptr<json> &input_json,
                     const std::shared_ptr<json> &output_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

   private:
    static void init();
};


#endif //RUSSEL_SYSTEM_RESOURCE_HPP
