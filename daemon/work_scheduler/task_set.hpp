//
// Created by einspaten on 21.05.20.
//

#ifndef RUSSEL_TASK_SET_HPP
#define RUSSEL_TASK_SET_HPP

#include <map>
#include <memory>
#include <russel-interpreter/routine/routine.hpp>
#include <vector>

#include "../routines/routine_analyzer/size_calculator.hpp"
#include "task.hpp"

auto find_name(const std::string& file_path) -> std::string;

class TaskSet {
 public:
  TaskSet(const std::shared_ptr<runtime::Routine>& routine,
          const std::string& data, const std::vector<unsigned int>& unset_vars);
  TaskSet(const std::string& routine_name, const std::string& data,
          const std::vector<unsigned int>& unset_vars);
  ~TaskSet();

  void load_from_file(const std::string& routine_path);

  /*!
   * @brief sets variable for later use or overwrites existing var
   * @param data
   * @param index
   * @param size
   */
  auto set_data(const std::shared_ptr<char[]>& data) -> std::shared_ptr<Task>;

  auto set_data(const std::string& data) -> std::shared_ptr<Task>;

  void set_required_vars(const std::vector<unsigned int>& req_vars);

  auto get_time_of_last_update() -> unsigned int;

 private:
  /*!
   * @brief generates static static sizes and creates prepared data field
   */
  void setup();

  auto routine_copy() -> std::shared_ptr<runtime::Routine>;

  unsigned int data_field_size_ = 0;

  std::shared_ptr<runtime::Routine> routine_;
  std::map<unsigned int, VariableInformation> variables_;  //
  std::string raw_data_;
  // Fully merged data field for runtime object
  std::shared_ptr<char[]> prepared_data_;

  // Variables that are required to be send
  std::vector<unsigned int> unset_variables_;
  std::vector<unsigned int> required_vars_;

  unsigned int time_of_last_update_;

  std::string routine_name_;
};

#endif  // RUSSEL_TASK_SET_HPP
