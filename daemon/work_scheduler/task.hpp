//
// Created by einspaten on 06.04.20.
//

#ifndef RUSSEL_TASK_HPP
#define RUSSEL_TASK_HPP

#include <memory>
#include <russel-interpreter/routine/routine.hpp>
#include <russel-interpreter/runtime/runtime.hpp>

#include "../addresser/murmur_hash.hpp"
#include "../information_object.hpp"

class Task : public internal_essential::InformationObject {
 public:
  Task(const std::shared_ptr<runtime::Routine> &routine,
       const std::shared_ptr<char[]> &data, unsigned int data_size);
  Task(const std::string &routine_name, const std::shared_ptr<char[]> &data,
       unsigned int data_size);
  Task(const std::string &routine_name,
       const std::shared_ptr<const char[]> &data, unsigned int data_size);
  Task(const std::string &routine_name, const std::string &data);

  ~Task();
  /*!
   * @brief serialize object for transmission
   * @param json_object where tha data will be put
   */
  void serialize(
      const std::shared_ptr<nlohmann::json>
          &json_object);  // TODO: for transmitting it to other engines
  /*!
   * @brief Runs Task
   */
  void run();
  /*!
   * @brief Serilizes it self and transmits it self to the place it originated
   * from set_network_stuff had to be called to work
   */
  void send_back();

  auto get_transmission_size() const -> unsigned int;
  auto get_estimated_workload() const -> unsigned int;
  auto done() -> bool;

  void set_network_stuff(int socket, unsigned short origin);
  void set_required_vars(const std::vector<unsigned int> &required_vars);
  void set_task_token(const std::string &token);
  void set_routine_path(const std::string& routine_path);

  void autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                   const std::shared_ptr<nlohmann::json> &output_json) override;

  auto hash() -> unsigned int override;

  void addToRuntime() override;

  void send_to_destination(int socket, unsigned short origin);

 private:
  void load_from_file(const std::string &path);

  // Important Factor for where the thing should be distributed
  unsigned int transmission_size_;
  unsigned int estimated_workload_;
  bool done_ = false;

  std::shared_ptr<char[]> data_;
  unsigned int data_size_;

  std::shared_ptr<runtime::Routine> routine_;
  std::shared_ptr<runtime::Runtime> this_runtime_;

  int socket_ = -1;
  unsigned short origin_ = 0;

  std::string task_token_ = "";

  std::vector<unsigned int>
      return_vars_;  // List of the variables that should be send back
  std::string routine_name_ = "";
  // TODO: needs runtime that needs to be executes

  unsigned int my_hash_value_;
};

#endif  // RUSSEL_TASK_HPP
