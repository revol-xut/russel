//
// Created by einspaten on 11.04.20.
//

#include "worker.hpp"

#include <thread>

auto Worker::get_network_speed_rating() -> unsigned int {
  return network_speed_rating_;
}

auto Worker::get_computational_power_rating() -> unsigned int {
  return computational_power_rating_;
}

auto Worker::get_queue() -> std::shared_ptr<TaskQueue> { return queue_; }

auto Worker::get_identity() -> bool { return identity_; }

auto Worker::new_stuff() -> bool { return new_stuff_; }

LocalWorker::LocalWorker() {
  running_ = true;
  identity_ = false;
  queue_ = std::make_shared<TaskQueue>(std::shared_ptr<Worker>(this));
}
LocalWorker::~LocalWorker() {
  running_ = false;
  mutex_.unlock();
  cv_.notify_all();
  thread_->join();

  computational_power_rating_ = 10;
  network_speed_rating_ = 100;
}

void LocalWorker::wake_up() {
  if (thread_ == nullptr) {
    thread_ = std::make_unique<std::thread>(&LocalWorker::loop, this);
  }

  if (running_) {  // Is local and not running
    new_stuff_ = true;
    mutex_.unlock();
    cv_.notify_all();
  }
}

void LocalWorker::loop() {
  while (running_) {
    std::unique_lock<std::mutex> lock(mutex_);

    while (not queue_->empty()) {
      auto task = queue_->get_task();

      if (task != nullptr) {
        task->run();
        task->send_back();
      } else {
        break;
      }
    }

    cv_.wait(lock);
  }
  running_ = false;  // Threads terminates here because it solved every task
}

NetworkWorker::NetworkWorker() {
  socket_ = -1;
  origin_ = 0;
  network_speed_rating_ = 10;
  computational_power_rating_ = 100;
  identity_ = true;
  queue_ = std::make_shared<TaskQueue>(std::shared_ptr<Worker>(this));
}

NetworkWorker::NetworkWorker(int socket, unsigned short origin) {
  socket_ = socket;
  origin_ = origin;
  network_speed_rating_ = 10;
  computational_power_rating_ = 100;
  identity_ = true;
  queue_ = std::make_shared<TaskQueue>(std::shared_ptr<Worker>(this));
}

NetworkWorker::~NetworkWorker() = default;

void NetworkWorker::wake_up() {
  auto json_object = std::make_shared<nlohmann::json>();
  auto json_data = std::make_shared<nlohmann::json>();

  while (not queue_->empty()) {
    auto task = queue_->get_task();
    task->send_to_destination(socket_, origin_);
  }
}

auto NetworkWorker::equivalent(int socket, unsigned short origin) const
    -> bool {
  return socket_ == socket and origin_ == origin;
}
