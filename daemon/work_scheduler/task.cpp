//
// Created by einspaten on 06.04.20.
//

#include "task.hpp"

#include <random>
#include <russel-interpreter/runtime/runtime.hpp>

#include "../network/socket.hpp"
#include "../other/base64.hpp"
#include "../routines/routine.hpp"
#include "../routines/routine_analyzer/workload_estimator.hpp"

Task::Task(const std::shared_ptr<runtime::Routine> &routine,
           const std::shared_ptr<char[]> &data, unsigned int data_size) {
  auto ptr = std::make_unique<WorkloadEstimator>(routine);
  ptr->generate_ratings();

  transmission_size_ =
      ptr->get_transmission_size();  // TODO: Maybe read from file and
  estimated_workload_ = ptr->get_estimated_workload();

  data_ = data;
  routine_ = routine;

  this_runtime_ = std::make_unique<runtime::Runtime>(routine_);

  done_ = false;
  socket_ = -1;
  origin_ = 0;
  data_size_ = data_size;
}

Task::Task(const std::string &routine_path, const std::shared_ptr<char[]> &data,
           unsigned int data_size) {  // TODO: Needs full path maybe request
                                      // base path from config
  data_ = data;

  load_from_file(routine_path);

  auto ptr = std::make_unique<WorkloadEstimator>(routine_);
  ptr->generate_ratings();

  transmission_size_ =
      ptr->get_transmission_size();  // TODO: Maybe read from file and
  estimated_workload_ = ptr->get_estimated_workload();

  this_runtime_ = std::make_unique<runtime::Runtime>(routine_);

  done_ = false;
  socket_ = -1;
  origin_ = 0;
  data_size_ = data_size;
  routine_name_ = routine_path;
}

Task::Task(const std::string &routine_path,
           const std::shared_ptr<const char[]> &data, unsigned int data_size) {
  data_ = std::shared_ptr<char[]>(static_cast<char *>((void *)data.get()));

  load_from_file(routine_path);

  auto ptr = std::make_unique<WorkloadEstimator>(routine_);
  ptr->generate_ratings();

  transmission_size_ =
      ptr->get_transmission_size();  // TODO: Maybe read from file and
  estimated_workload_ = ptr->get_estimated_workload();

  this_runtime_ = std::make_unique<runtime::Runtime>(routine_);

  done_ = false;
  socket_ = -1;
  origin_ = 0;
  data_size_ = data_size;
  routine_name_ = routine_path;
}

Task::Task(const std::string &routine_path, const std::string &data) {
  load_from_file(routine_path);

  auto ptr = std::make_unique<WorkloadEstimator>(routine_);
  ptr->generate_ratings();

  transmission_size_ =
      ptr->get_transmission_size();  // TODO: Maybe read from file and
  estimated_workload_ = ptr->get_estimated_workload();
  done_ = false;
  socket_ = -1;
  origin_ = 0;

  unsigned int size = data.size();
  auto temp_data = new char[size + 1];
  std::memcpy(temp_data, data.c_str(), size * sizeof(char));
  data_ = std::shared_ptr<char[]>(temp_data);

  data_size_ = size;
  routine_name_ = routine_path;
}

void Task::load_from_file(const std::string &path) {
  std::string base_path = runtime_->getConfig()->getString("routine_path");
  auto temp = std::make_shared<Routine>();
  temp->load(base_path + path);
  routine_ = temp->getRoutine();
  routine_name_ = path;
}

Task::~Task() = default;

void Task::serialize(const std::shared_ptr<nlohmann::json> &json_object) {
  (*json_object)["routine_name"] = routine_name_;
  (*json_object)["required_vars"] = return_vars_;
  std::string raw_data =
      std::string(data_.get(), data_size_);
  std::string encoded;

  Base64::encode(raw_data, encoded);
  (*json_object)["data"] = encoded;
}

void Task::run() {
  if (this_runtime_ == nullptr) {
    this_runtime_ = std::make_shared<runtime::Runtime>(routine_);
  }

  this_runtime_->setData(data_, data_size_);
  try {
    this_runtime_->execute();
    done_ = true;
  } catch (const std::exception &e) {  // TODO: Create Log entries
    std::cerr << e.what() << std::endl;
  }
}

auto Task::done() -> bool { return done_;
}

auto Task::get_transmission_size() const -> unsigned int {
  return transmission_size_;
}

auto Task::get_estimated_workload() const -> unsigned int {
  return estimated_workload_;
}

auto Task::hash() -> unsigned int {
    internal_essential::MurmurHash murmur_hash{};
    murmur_hash.Begin();
    auto address = reinterpret_cast<std::size_t>(data_.get());

    murmur_hash.Add((const unsigned char *)(&address), sizeof(address));
    murmur_hash.Add((const unsigned char *)(&socket_), sizeof(socket_));
    murmur_hash.Add((const unsigned char *) (&origin_), sizeof(origin_));
    murmur_hash.Add((const unsigned char *) (&estimated_workload_), sizeof(estimated_workload_));

    return murmur_hash.End();
}

void Task::send_back() {
  if (objects_.find(0) != objects_.end() and done_) {
    auto input_json = std::make_shared<nlohmann::json>();
    auto output_json = std::make_shared<nlohmann::json>();

    (*input_json)["command"] = "send_message";
    (*input_json)["socket"] = socket_;
    (*input_json)["origin"] = origin_;

    if (this_runtime_->get_exit_status() < 0) {
      std::cerr << "execution of runtime failed"
                << this_runtime_->get_errors().size() << std::endl;

      unsigned int error_count = 0;
      for (const std::shared_ptr<runtime::Error> &error :
           this_runtime_->get_errors()) {
        (*input_json)["error_" + std::to_string(error_count)] = error->message;
        std::cerr << "Error Information: " << error->message
                  << " Byte Code position: " << error->byte_code_position
                  << " error code: " << error->error_code << std::endl;
        std::cerr << "Failed Command: " << (short)error->failed_command.command
                  << std::endl;
      }
      nlohmann::json json_data;

      json_data["command"] = "task_return_error";
      json_data["token"] = task_token_;

      (*input_json)["data"] = json_data.dump();

      objects_.at(0)->autoRespond(input_json, output_json);
    }

    nlohmann::json json_data;

    datatypes::AbstractType *abstractType = nullptr;
    char *data = nullptr;

    for (const unsigned int var : return_vars_) {
      if (this_runtime_->getVariableCount() <= var) {
        std::cerr << "[DAEMON] No variable found that can be returned"
                  << std::endl;

        json_data["command"] = "task_return_error";
        json_data["token"] = task_token_;

        (*input_json)["data"] = json_data.dump();

        objects_.at(0)->autoRespond(input_json, output_json);
        return;
      }

      abstractType = &this_runtime_->getVariable(var);

      data = new char[abstractType->getSize()];
      abstractType->serializeObject(data);

      std::string output;
      Base64::encode(std::string(data, abstractType->getSize()), output);
      json_data[std::to_string(var)] = output;

      delete[] data;
    }
    json_data["command"] = "task_return";
    json_data["token"] = task_token_;

    (*input_json)["data"] = json_data.dump();

    objects_.at(0)->autoRespond(input_json, output_json);
  }
}

void Task::addToRuntime() {}

void Task::autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                       const std::shared_ptr<nlohmann::json> &output_json) {

    std::string command = input_json->at("command");

    if (command == "info") {
        (*output_json)["transmission_size_"] = transmission_size_;
        (*output_json)["estimated_workload_"] = estimated_workload_;
    }
}

void Task::set_network_stuff(int socket, unsigned short origin) {
  socket_ = socket;
  origin_ = origin;
}

void Task::set_required_vars(const std::vector<unsigned int> &required_vars) {
  return_vars_ = required_vars;
}

void Task::set_task_token(const std::string &token) { task_token_ = token; }

void Task::send_to_destination(int socket, unsigned short origin) {
  if (runtime_ == nullptr or objects_.find(0) == objects_.end()) {
    return;
  }
  auto json_ptr = std::make_shared<nlohmann::json>();
  serialize(json_ptr);

  auto input_json = std::make_shared<nlohmann::json>();
  auto output_json = std::make_shared<nlohmann::json>();

  (*input_json)["command"] = "send_message";
  (*input_json)["socket"] = socket;
  (*input_json)["origin"] = origin;

  auto json_data = std::make_shared<nlohmann::json>();

  (*json_data)["command"] = "api";
  (*json_data)["id"] = 23;

  (*json_data)["data"] = (*json_ptr);
  (*input_json)["data"] = json_data->dump();

  objects_.at(0)->autoRespond(input_json, output_json);

  auto json_task_forwarder = std::make_shared<nlohmann::json>();
  (*json_task_forwarder)["command"] = "add_waiting_task";
  (*json_task_forwarder)["socket"] = socket;
  (*json_task_forwarder)["origin"] = origin;
  (*json_task_forwarder)["token"] = task_token_;

  std::cout << *json_task_forwarder << std::endl;
  objects_.at(24)->autoRespond(json_task_forwarder, output_json);
}

void Task::set_routine_path(const std::string &routine_path) {
    routine_name_ = routine_path;
}