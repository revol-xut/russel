//
// Created by einspaten on 06.04.20.
//

#ifndef RUSSEL_SCHEDULER_ENGINE_HPP
#define RUSSEL_SCHEDULER_ENGINE_HPP

#include <vector>

#include "../information_object.hpp"
#include "../network/socket.hpp"
#include "hungarian_algo.hpp"
#include "task.hpp"
#include "task_set.hpp"
#include "worker.hpp"

constexpr unsigned int kScheduleEngineID = 23;
constexpr unsigned int kDeleteTaskSetTimer = 20;  // seconds

class SchedulerEngine : public internal_essential::InformationObject {
 public:
  SchedulerEngine();
  ~SchedulerEngine();

  void autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                   const std::shared_ptr<nlohmann::json> &output_json) override;

  auto hash() -> unsigned int override;

  void addToRuntime() override;

  void remove_unused_task_sets();

 private:
  /*!
   * @brief Adds new task to pending task which will be scheduled
   * @param new_task
   */
  void register_task(const std::shared_ptr<Task> &new_task);
  /*!
   * @brief loads configuration and creates thread workers
   */
  void thread_workers();
 /*!
  * @brief schedules tasks but only how many workers are connected
  */
 void schedule();

 auto random_number() -> unsigned int;

 unsigned int batch_size_, last_schedule_, update_timer_;

 std::vector<std::shared_ptr<Task>> tasks_;
 std::vector<std::shared_ptr<Worker>> available_workers_;
 std::map<unsigned int, std::shared_ptr<TaskSet>> task_sets_;
};


#endif //RUSSEL_SCHEDULER_ENGINE_HPP
