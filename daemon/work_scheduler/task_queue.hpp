//
// Created by einspaten on 11.04.20.
//

#ifndef RUSSEL_TASK_QUEUE_HPP
#define RUSSEL_TASK_QUEUE_HPP

#include <vector>

#include "task.hpp"

class Worker;

class TaskQueue {
 public:
  TaskQueue();
  explicit TaskQueue(const std::shared_ptr<Worker> &worker);
  ~TaskQueue();

  /*!
   * @brief Adds given task to queue and wakes up his worker
   * @param task
   */
  void push_back(const std::shared_ptr<Task> &task);
  /*!
   * @brief Gets lowest task in the queue
   * @return
   */
  auto get_task() -> std::shared_ptr<Task>;

  /*!
   * @brief Function for the work balancer returns task which fits given worker
   * config the best
   * @param comp_power
   * @param network_speed
   * @return
   */
  auto get_highest_synergy(unsigned int comp_power, unsigned int network_speed)
      -> std::shared_ptr<Task>;

  auto empty() -> bool;
  auto size() -> unsigned int;

 private:
  std::vector<std::shared_ptr<Task>> tasks_;
  std::shared_ptr<Worker> worker_;

  static std::vector<std::shared_ptr<TaskQueue>> all_task_queues_;
};

#include "worker.hpp"

#endif //RUSSEL_TASK_QUEUE_HPP
