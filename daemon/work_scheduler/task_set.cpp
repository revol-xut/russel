//
// Created by einspaten on 21.05.20.
//

#include "task_set.hpp"

#include "../routines/routine.hpp"

auto find_name(const std::string& file_path) -> std::string{
    // Takes a Unix absolute file path and spits our the file name
    std::stringstream ss(file_path);
    std::string token;

    auto index_position = std::count(file_path.begin(), file_path.end(), '/');
    unsigned int enumerator = 0;

    while (std::getline(ss, token, '/')) {
        if (enumerator == index_position){
            return token;
        }
        enumerator++;
    }
    return "";
}


TaskSet::TaskSet(const std::shared_ptr<runtime::Routine> &routine,
                 const std::string &data,
                 const std::vector<unsigned int> &unset_vars) {
  routine_ = routine;
  raw_data_ = data;
  unset_variables_ = unset_vars;
  setup();
  time_of_last_update_ = std::time(nullptr);
}

TaskSet::TaskSet(const std::string &routine_path, const std::string &data,
                 const std::vector<unsigned int> &unset_vars) {
  load_from_file(routine_path);
  raw_data_ = data;
  unset_variables_ = unset_vars;
  setup();
  time_of_last_update_ = std::time(nullptr);
}

TaskSet::~TaskSet() {
  unset_variables_.clear();
  required_vars_.clear();

  variables_.clear();
};

void TaskSet::load_from_file(const std::string &routine_path) {
  time_of_last_update_ = std::time(nullptr);

  auto temp = std::make_shared<Routine>();
  temp->load(routine_path);
  routine_ = temp->getRoutine();
  routine_name_ = find_name(routine_path);
}

void TaskSet::setup() {
  time_of_last_update_ = std::time(nullptr);

  SizeCalculator calculator(routine_, raw_data_);
  calculator.generate();

  for (unsigned int var : unset_variables_) {
    variables_[var] = calculator.get_variable(var);
  }

  auto temporary = new char[calculator.get_data_field_size()];
  unsigned int current_mem_offset = 0;
  VariableInformation var_info{};

  for (unsigned int i = 0; i < calculator.get_variable_count(); i++) {
    var_info = calculator.get_variable(i);

    if (std::find(unset_variables_.begin(), unset_variables_.end(), i) ==
        unset_variables_
            .end()) {  // Found Variable and copies it into the right location
      memcpy(temporary + var_info.memory_position,                    // NOLINT
             raw_data_.c_str() + current_mem_offset, var_info.size);  // NOLINT
      current_mem_offset += var_info.size;
    }
  }
  prepared_data_ = std::shared_ptr<char[]>(temporary);
  data_field_size_ = calculator.get_data_field_size();
}

auto TaskSet::set_data(const std::shared_ptr<char[]> &data)
    -> std::shared_ptr<Task> {
  time_of_last_update_ = std::time(nullptr);

  // Initializes data-field and copy static variables into it.
  auto data_copy = std::make_shared<char[]>(data_field_size_);
  std::memcpy(data_copy.get(), prepared_data_.get(), data_field_size_);

  // Now fills the empty spots in the data field with the non static variables
  VariableInformation var_info{};
  unsigned int current_mem_pos = 0;

  for (unsigned int var : unset_variables_) {
    var_info = variables_.at(var);

    std::memcpy(data_copy.get() + var_info.memory_position,
                data.get() + current_mem_pos, var_info.size);  // NOLINT
    current_mem_pos += var_info.size;
  }

  auto task = std::make_shared<Task>(routine_, data_copy, data_field_size_);
  task->set_required_vars(required_vars_);
  task->set_routine_path(routine_name_);

  return task;
}

auto TaskSet::set_data(const std::string &data) -> std::shared_ptr<Task> {
  time_of_last_update_ = std::time(nullptr);

  // Initializes data-field and copy static variables into it.
  auto data_copy = new char[data_field_size_ + 1];
  std::memcpy(data_copy, prepared_data_.get(), data_field_size_);

  // Now fills the empty spots in the data field with the non static variables
  VariableInformation var_info{};
  unsigned int current_mem_pos = 0;

  for (unsigned int var : unset_variables_) {
    var_info = variables_.at(var);

    std::memcpy(data_copy + var_info.memory_position,
                data.c_str() + current_mem_pos, var_info.size);  // NOLINT
    current_mem_pos += var_info.size;
  }

  auto smart_ptr = std::shared_ptr<char[]>(data_copy);
  auto routine_copy =
      this->routine_copy();  // Is necessary to keep current pos tracker

  auto task = std::make_shared<Task>(routine_copy, smart_ptr, data_field_size_);
  task->set_required_vars(required_vars_);
  task->set_routine_path(routine_name_);

  return task;
}

void TaskSet::set_required_vars(const std::vector<unsigned int> &req_vars) {
  time_of_last_update_ = std::time(nullptr);

  required_vars_ = req_vars;
}

auto TaskSet::routine_copy() -> std::shared_ptr<runtime::Routine> {
  time_of_last_update_ = std::time(nullptr);

  return std::make_shared<runtime::Routine>(routine_->getData(),
                                            routine_->getSize());
}

auto TaskSet::get_time_of_last_update() -> unsigned int {
  return std::time(nullptr) - time_of_last_update_;
}