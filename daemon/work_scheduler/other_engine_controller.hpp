//
// Created by einspaten on 29.03.20.
//

#ifndef RUSSEL_OTHER_ENGINE_CONTROLLER_HPP
#define RUSSEL_OTHER_ENGINE_CONTROLLER_HPP

#include <memory>
#include <nlohmann/json.hpp>
#include <tuple>

#include "../information_object.hpp"
#include "../network/socket.hpp"
#include "./system_resource.hpp"

constexpr unsigned int kOtherEngineControllerId = 22;
constexpr unsigned int kMinUpdateTimer = 5;  // In Seconds
constexpr const char *kMessageRequestTemplate = "{'command':'workload'}";

struct NetworkEntityInformation {
  unsigned int last_update;
  unsigned int work_load_estimate;
  bool is_russel_engine;
};

class OtherEngineController : public internal_essential::InformationObject {
 public:
  OtherEngineController();

  ~OtherEngineController();

  void autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                   const std::shared_ptr<nlohmann::json> &output_json) override;

  auto hash() -> unsigned int override;

  void addToRuntime() override;

  void send_heart_beat();

  auto requires_update() -> bool;

 private:
  // This is anticipated to work like a heart but also transport useful
  // information
  std::map<std::tuple<int, unsigned int>,
           std::shared_ptr<NetworkEntityInformation>>
      network_entities_;

  unsigned int last_update_;
};


#endif //RUSSEL_OTHER_ENGINE_CONTROLLER_HPP
