//
// Created by einspaten on 08/07/2020.
//

#ifndef RUSSEL_GLOBAL_MEMORY_MANAGEMENT_HPP
#define RUSSEL_GLOBAL_MEMORY_MANAGEMENT_HPP

#include <cstring>
#include <memory>
#include <map>

constexpr unsigned int kGarbageCollectorTime = 20;

struct MemoryChunk{
    std::shared_ptr<char[]> data_;
    unsigned int size_;
    unsigned int last_interaction_;
};

class GlobalMemoryManagement {
public:
    GlobalMemoryManagement();
    ~GlobalMemoryManagement();

    auto add_data(const std::string& data) -> unsigned int;
    auto add_data(const std::shared_ptr<char[]>& data, unsigned int size) -> unsigned int;
    auto add_data(const std::shared_ptr<MemoryChunk>& memory_chunk) -> unsigned int;

    auto id_exists(unsigned int id) -> bool;

    auto get_chunk_data(unsigned int id) -> std::shared_ptr<MemoryChunk>;
    auto get_raw_data(unsigned int id) -> const char*;
    auto get_smart_ptr_data(unsigned int id) -> std::shared_ptr<char[]>;

    void check_deallocation();
    void deallocate(unsigned int id);

private:

    auto generate_random_id() -> unsigned int;

    std::map<unsigned int, std::shared_ptr<MemoryChunk>> data_;

};



#endif //RUSSEL_GLOBAL_MEMORY_MANAGEMENT_HPP
