//
// Created by einspaten on 30.12.19.
//

#ifndef RUSSEL_APPLICATION_CONSTRUCTOR_HPP
#define RUSSEL_APPLICATION_CONSTRUCTOR_HPP

#include <memory>

#include "../execution/execution_handler.hpp"
#include "../work_scheduler/other_engine_controller.hpp"
#include "../work_scheduler/scheduler_engine.hpp"
#include "../work_scheduler/system_resource.hpp"
#include "../work_scheduler/task_forwarder.hpp"
#include "./garbage_collector.hpp"

class ApplicationConstructor {
public:
 /*!
  * This Class is the heart piece it injects the runtime object carrier in all
  * relevant things So a global variable is unnecessary and objects are more
  * testable.
  */
 ApplicationConstructor();
 ~ApplicationConstructor();

 static void create_runtime();

 static void check_file_structure();

 void send_heart_beat();

 void collect_garbage();

private:
 std::unique_ptr<ExecutionHandler> execution_handler_;
 std::unique_ptr<SystemResource> system_resources_;
 std::unique_ptr<OtherEngineController> other_engine_controller_;
 std::unique_ptr<SchedulerEngine> scheduler_engine_;
 std::unique_ptr<TaskForwarder> task_forwarder_;
 std::unique_ptr<GarbageCollector> garbage_collector_;
};


#endif //RUSSEL_APPLICATION_CONSTRUCTOR_HPP
