//
// Created by revol-xut on 11/1/19.
//

#ifndef WORKER_RUNTIME_HPP
#define WORKER_RUNTIME_HPP

#include <memory>
#include <nlohmann/json.hpp>

#include "../config/config.hpp"
#include "../config/network_conf.hpp"
#include "../logging/logger.hpp"
#include "./global_memory_management.hpp"

//Default Config and Logging Files
#define DEFAULT_CONFIG "/etc/russel/russel.json" //NOLINT
#define DEFAULT_LOG "/etc/russel/russel.log" //NOLINT

//Default Logging Levels
constexpr int FATAL = 4;
constexpr int ERROR = 3;
constexpr int DEBUG = 2;
constexpr int INFO = 1;

class Runtime {

    //TODO: Use Dependency Injection to place this object in the relevant places. Therefore build a Application Constructor which sets the entire things

public:
    /*!
     * @brief Default Constructor takes defined default config file paths
     */
    explicit Runtime();

    /*!
     * @brief Creates runtime object with given paths to process relevant files like logging and configuration
     * @param path_config Config File
     * @param path_logger Logging File
     */
    explicit Runtime(const std::string &path_config, const std::string &path_logger);


    ~Runtime();

    /*!
     * @brief Returns Logger for corresponding logging level
     * @param level 4 - 1 its recommended to take defined Macros
     * @return Logger reference
     */
    auto getLogger(int level) -> internal_essential::Logger &;

    /*!
     * @brief Returns default Config object
     * @return Config object
     */
    auto getConfig() -> std::shared_ptr<internal_essential::Config>;

    /*!
     * @brief Returns the Config object which contains network information
     * @return Config object
     */
    auto getNetworkConfig() -> std::shared_ptr<internal_essential::NetworkConfig> &;

    /*!
     * @brief Gets status of daemon so it can swiftly exit
     * @return Returns if the the daemon should be terminated
     */
    auto getRunning() -> bool;

    /*!
     * @brief Starts the exiting process
     */
    void terminate();

    /*
     * @brief Returns Global Memory Management
     */
    auto get_global_memory() -> std::shared_ptr<GlobalMemoryManagement>;

private:
    // General Config
    std::shared_ptr<internal_essential::Config> config_;
    // Network Config
    std::shared_ptr<internal_essential::NetworkConfig> network_config_;
    // Shared Memory
    std::shared_ptr<GlobalMemoryManagement> global_memory_;


    internal_essential::Logger logger_;
    bool running_;
};


#endif //WORKER_RUNTIME_HPP
