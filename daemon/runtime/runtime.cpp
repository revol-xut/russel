//
// Created by revol-xut on 11/1/19.
//

#include "runtime.hpp"

Runtime::Runtime() {

    logger_.setFile(DEFAULT_LOG);
    logger_.setStatus("INFO");

    config_ = std::make_shared<internal_essential::Config>(DEFAULT_CONFIG);
    global_memory_ = std::make_shared<GlobalMemoryManagement>();

    network_config_ = std::make_shared<internal_essential::NetworkConfig>(config_->getString("networkConf"));
    running_ = true;
}

Runtime::Runtime(const std::string &path_config, const std::string &path_logger) {
    logger_.setFile(path_logger);
    logger_.setStatus("INFO");

    config_ = std::make_shared<internal_essential::Config>(path_config);
    global_memory_ = std::make_shared<GlobalMemoryManagement>();

    network_config_ = std::make_shared<internal_essential::NetworkConfig>(config_->getString("networkConf"));
    running_ = true;
}

Runtime::~Runtime() = default;

auto Runtime::getLogger(int level) -> internal_essential::Logger & {
    logger_.setStatus(level);
    return logger_;
}

auto Runtime::getConfig() -> std::shared_ptr<internal_essential::Config> {
    return config_;
}

auto Runtime::getNetworkConfig() -> std::shared_ptr<internal_essential::NetworkConfig> & {
    return network_config_;
}

auto Runtime::getRunning() -> bool {
    return running_;
}

void Runtime::terminate() {
    running_ = false;
}

auto Runtime::get_global_memory() -> std::shared_ptr<GlobalMemoryManagement> {
    return global_memory_;
}