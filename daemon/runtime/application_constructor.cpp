//
// Created by einspaten on 30.12.19.
//

#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <experimental/filesystem>
#include <filesystem>

#include "application_constructor.hpp"
#include "../information_object.hpp"

ApplicationConstructor::ApplicationConstructor() {
    std::string config_path = "/etc/russel/";

    if (not std::experimental::filesystem::exists(config_path)) {
        throw std::runtime_error("Config folder does not exists !");
    }
    // Read config and start application
    create_runtime();
    check_file_structure();

    execution_handler_ = std::make_unique<ExecutionHandler>();
    system_resources_ = std::make_unique<SystemResource>();
    other_engine_controller_ = std::make_unique<OtherEngineController>();
    scheduler_engine_ = std::make_unique<SchedulerEngine>();
    task_forwarder_ = std::make_unique<TaskForwarder>();
    garbage_collector_ = std::make_unique<GarbageCollector>();
}

ApplicationConstructor::~ApplicationConstructor() = default;

void ApplicationConstructor::check_file_structure() {
    std::string routine_path = internal_essential::InformationObject::getRuntime()->getConfig()->getString(
            "routine_path");


    if (routine_path.empty()) {
        routine_path = "/etc/russel/routines/";
    }
    if (not std::experimental::filesystem::exists(routine_path)) {
        // Creates folder when it does not exists
        std::experimental::filesystem::create_directories(routine_path);
    }
    if (not std::experimental::filesystem::is_directory(routine_path)) {
      // When file is not a directory
      throw std::runtime_error("routine path is not a directory !");
    }
}

void ApplicationConstructor::create_runtime() {
  // Injects Runtime Object
  internal_essential::InformationObject::setRuntime(
      std::make_shared<Runtime>());
}

void ApplicationConstructor::send_heart_beat() {
  if (other_engine_controller_->requires_update()) {
    other_engine_controller_->send_heart_beat();
  }
}

void ApplicationConstructor::collect_garbage() {
  if (garbage_collector_ != nullptr) {
    garbage_collector_->run();
  }
}