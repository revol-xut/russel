//
// Created by einspaten on 25.05.20.
//

#ifndef RUSSEL_GARBAGE_COLLECTOR_HPP
#define RUSSEL_GARBAGE_COLLECTOR_HPP

#include "../information_object.hpp"

constexpr unsigned int kGarbageCollectorID = 40;
constexpr unsigned int kCollectGarbageTimer = 10;  // seconds

class GarbageCollector : public internal_essential::InformationObject {
 public:
  GarbageCollector();
  ~GarbageCollector();

  void autoRespond(const std::shared_ptr<nlohmann::json> &input_json,
                   const std::shared_ptr<nlohmann::json> &output_json) override;

  auto hash() -> unsigned int override;

  void addToRuntime() override;

  void run();

 private:
  unsigned int last_check_ = 0;
  unsigned int wait_time_ = 0;
};

#endif  // RUSSEL_GARBAGE_COLLECTOR_HPP
