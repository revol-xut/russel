//
// Created by einspaten on 08/07/2020.
//

#include "global_memory_management.hpp"

GlobalMemoryManagement::GlobalMemoryManagement() {
    data_ = {};
    srand( (unsigned)time(NULL));
}

GlobalMemoryManagement::~GlobalMemoryManagement() = default;

auto GlobalMemoryManagement::add_data(const std::string &data) -> unsigned int {

    auto temporary = new char[data.size()];
    std::memcpy(temporary, data.c_str(), data.size());

    auto new_object = std::make_shared<MemoryChunk>();
    new_object->data_ = std::shared_ptr<char[]>(temporary); //TODO: Hopefully copies that shit and I dont have to perform a memcpy
    new_object->size_ = data.size();
    new_object->last_interaction_ = time(nullptr);

    auto id = generate_random_id();
    data_[id] = new_object;

    return id;
}

auto GlobalMemoryManagement::add_data(const std::shared_ptr<char[]> &data, unsigned int size) -> unsigned int {

    auto new_object = std::make_shared<MemoryChunk>();
    new_object->data_ = data; //TODO: Hopefully copies that shit
    new_object->size_ = size;
    new_object->last_interaction_ = time(nullptr);

    auto id = generate_random_id();
    data_[id] = new_object;

    return id;
}

auto GlobalMemoryManagement::add_data(const std::shared_ptr<MemoryChunk> &memory_chunk) -> unsigned int {
    auto id = generate_random_id();
    data_[id] = memory_chunk;

    return id;
}

auto GlobalMemoryManagement::id_exists(unsigned int id) -> bool {
    return data_.find(id) != data_.end();
}

void GlobalMemoryManagement::check_deallocation() {
    for (const std::pair<unsigned int, std::shared_ptr<MemoryChunk>> mem_chunk: data_){
        if(mem_chunk.second->last_interaction_ - time(nullptr) > kGarbageCollectorTime){
            data_.erase(mem_chunk.first);
        }
    }
}

void GlobalMemoryManagement::deallocate(unsigned int id) {
    data_.erase(id);
}

auto GlobalMemoryManagement::get_chunk_data(unsigned int id) -> std::shared_ptr<MemoryChunk> {
    auto data = data_.at(id);
    data->last_interaction_ = time(nullptr);
    return data;
}

auto GlobalMemoryManagement::get_raw_data(unsigned int id) -> const char * {
    auto data = data_.at(id);
    data->last_interaction_ = time(nullptr);
    return data->data_.get();
}

auto GlobalMemoryManagement::get_smart_ptr_data(unsigned int id) -> std::shared_ptr<char []> {
    auto data = data_.at(id);
    data->last_interaction_ = time(nullptr);
    return data->data_;
}

auto GlobalMemoryManagement::generate_random_id() -> unsigned int {
    return static_cast<unsigned int>(rand());
}