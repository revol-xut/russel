//
// Created by einspaten on 25.05.20.
//

#include "garbage_collector.hpp"

GarbageCollector::GarbageCollector() {
  wait_time_ = runtime_->getConfig()->getInteger("garbage_collector_timer");
  last_check_ = std::time(nullptr);
  addToRuntime();
}

GarbageCollector::~GarbageCollector() = default;

void GarbageCollector::autoRespond(
    const std::shared_ptr<nlohmann::json> &input_json,
    const std::shared_ptr<nlohmann::json> &output_json) {
  std::string command = input_json->at("command");

  if (command == "update") {
    run();

    (*output_json)["success"] = true;
  }
}

void GarbageCollector::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<GarbageCollector>(this);
  }
}

auto GarbageCollector::hash() -> unsigned int { return kGarbageCollectorID; }

void GarbageCollector::run() {
  if (std::time(nullptr) - last_check_ < kCollectGarbageTimer) {
    return;
  }

  constexpr unsigned int kSchedulerEngine = 23;

  if (runtime_ != nullptr and
      objects_.find(kSchedulerEngine) != objects_.end()) {
    auto input_json = std::make_shared<nlohmann::json>();
    auto output_json = std::make_shared<nlohmann::json>();

    (*input_json)["command"] = "remove_dead_task_sets";
    objects_.at(kSchedulerEngine)->autoRespond(input_json, output_json);
  }

  last_check_ = std::time(nullptr);
}