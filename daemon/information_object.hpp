//
// Created by revol-xut on 12/13/19.
//

#ifndef RUSSEL_INFORMATION_OBJECT_HPP
#define RUSSEL_INFORMATION_OBJECT_HPP

#include <memory>
#include <nlohmann/json.hpp>
#include <string>

#include "addresser/murmur_hash.hpp"
#include "runtime/runtime.hpp"

namespace internal_essential {

using json = nlohmann::json;

/*!
 * @brief Classes inherit and implement this class and are as a result capable
 * of interacting with other engines or users
 */
class InformationObject {
 public:
  virtual void addToRuntime() = 0;

  /*!
   * @brief Represents the interface though every object can communicate with
   * the internal addressing system
   * @param input_json Contains information about what should be done
   * @param output_json return information
   */
  virtual void autoRespond(const std::shared_ptr<json> &input_json,
                           const std::shared_ptr<json> &output_json) = 0;

  /*!
   * @brief Creates identifier for this object
   * @return hash as unsigned integer
   */
  virtual auto hash() -> unsigned int = 0;

  /*!
   * @brief Sets runtime object
   * @param runtime
   */
  static void setRuntime(const std::shared_ptr<Runtime> &runtime);

  /*!
   * @brief Returns runtime object
   * @return Runtime Objects
   */
  static auto getRuntime() -> std::shared_ptr<Runtime> &;

  /*!
   * @brief Returns Objects from addressing system with this key or hash value;
   * @param index
   * @return
   */
  static auto getObject(unsigned int index)
      -> std::shared_ptr<InformationObject>;

 protected:
  static std::shared_ptr<Runtime> runtime_;
  static std::map<unsigned int, std::shared_ptr<InformationObject>> objects_;
};

}  // namespace internal_essential

#endif  // RUSSEL_INFORMATION_OBJECT_HPP
