//
// Created by revol-xut on 02.01.20.
//

#include "information_object.hpp"

std::map<unsigned int, std::shared_ptr<internal_essential::InformationObject>> internal_essential::InformationObject::objects_ = {};
std::shared_ptr<Runtime> internal_essential::InformationObject::runtime_ = nullptr;

void internal_essential::InformationObject::setRuntime(const std::shared_ptr<Runtime> &runtime) {
    runtime_ = runtime;
}

auto internal_essential::InformationObject::getRuntime() -> std::shared_ptr<Runtime> & {
    return runtime_;
}

auto internal_essential::InformationObject::getObject(
        unsigned int index) -> std::shared_ptr<internal_essential::InformationObject> {
    return objects_.at(index);
}

