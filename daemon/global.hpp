//
// Created by revol-xut on 11/2/19.
//

#ifndef WORKER_GLOBALS_HPP
#define WORKER_GLOBALS_HPP

#include <memory>

#include "runtime/runtime.hpp"

/*
 * This File contains all relevant global objects:
 * This Global variable ONLY get used by the unix signal handler to log events
 */

static std::unique_ptr<Runtime> const kRuntime = std::make_unique<Runtime>();

#endif