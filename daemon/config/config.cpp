//
// Created by revol-xut on 11/1/19.
//

#include "config.hpp"

#include <experimental/filesystem>
#include <iostream>

internal_essential::Config::Config() {
    config_ = {};
    setDefaultValues();
    config_path_ = "";
}

internal_essential::Config::Config(const std::string &path) {
    config_ = {};
    config_path_ = path;
    reload();
}

internal_essential::Config::Config(const std::string &path, bool load) {
    config_ = {};
    config_path_ = path;
    if (load) {
        reload();
    }
}

internal_essential::Config::~Config() = default;

void internal_essential::Config::setDefaultValues() {
    //TODO: Set all default Configs
    // Network Config
    if (not keyExists("networkConf")) {
        config_["networkConf"] = "/etc/russel/network.json";
    }
    if (not keyExists("syslog")) {
      config_["syslog"] = true;
    }
    if (not keyExists("routine_path")) {
      config_["routine_path"] = "/etc/russel/routines/";
    }
    if (not keyExists("scheduler_update")) {
      config_["scheduler_update"] = 5;
    }
    if (not keyExists("thread_workers")) {
      config_["thread_workers"] = 3;
    } else {
      if (config_["thread_workers"] <= 0) {
        throw std::runtime_error(
            "It is not possible to have a russel daemon with no worker");
      }
    }
    if (not keyExists("garbage_collector_timer")) {
      config_["garbage_collector_timer"] =
          10;  // 10 second of inactivity and object will be removed from the
               // system
    }
}

void internal_essential::Config::readFile(const std::string &file) {
  if (std::experimental::filesystem::exists(file)) {
    std::ifstream stream_file;
    stream_file.open(file);
    stream_file >> config_;
  }
}

auto internal_essential::Config::getString(const std::string &key) -> std::string {
    if (config_.find(key) != config_.end()) {
        return config_.at(key);
    }
    return "";
}

auto internal_essential::Config::getInteger(const std::string &key) -> int {
    if (config_.find(key) != config_.end()) {
        return config_.at(key);
    }
    return -1;
}

auto internal_essential::Config::getBool(const std::string &key) -> bool {
    if (config_.find(key) != config_.end()) {
        return config_.at(key);

    }
    return false;
}

void internal_essential::Config::reload() {
    if (!config_path_.empty()) {
        readFile(config_path_);
    }
    setDefaultValues();
}

void internal_essential::Config::setValueString(const std::string &key, const std::string &value) {
    config_[key] = value;
}

bool internal_essential::Config::keyExists(const std::string &key) {
    return not(config_.find(key) == config_.end());
}