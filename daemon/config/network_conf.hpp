//
// Created by revol-xut on 12/8/19.
//

#ifndef RUSSEL_NETWORK_CONF_HPP
#define RUSSEL_NETWORK_CONF_HPP


#include "config.hpp"

namespace internal_essential {
    /*!
     * @brief This class overrides the defaultValue function from the default Config setting the network relevant values
     */
    class NetworkConfig : public Config {
    public:

        explicit NetworkConfig(const std::string &path);

        explicit NetworkConfig();

        ~NetworkConfig() override;

        void setDefaultValues() override;

    };

} //namespace internal_essential


#endif //RUSSEL_NETWORK_CONF_HPP
