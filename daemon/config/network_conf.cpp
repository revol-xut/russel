//
// Created by revol-xut on 12/8/19.
//

#include "network_conf.hpp"

internal_essential::NetworkConfig::NetworkConfig(const std::string &path) {
    config_ = {};
    //setDefaultValues();
    setConfig(path);
    reload();
}

internal_essential::NetworkConfig::NetworkConfig() {
    config_ = {};
    setConfig("");
    setDefaultValues();
}


void internal_essential::NetworkConfig::setDefaultValues() {

    // Which communication ways are enabled
    if (not keyExists("enable_ipc")) {
        config_["enable_ipc"] = true;
    }
    if (not keyExists("enable_pub")) {
        config_["enable_pub"] = true;
    }
    if (not keyExists("enable_cli")) {
        config_["enable_cli"] = true;
    }

    // Public Socket Server
    if (not keyExists("host")) {
        config_["host"] = "127.0.0.1";
    }
    if (not keyExists("port")) {
        config_["port"] = 8321;
    }

    // Ipc Socket Server
    if (not keyExists("ipc")) {
      config_["ipc"] = "/run/russel.sock";
    }

    // Auto-Connect feature
    if (not keyExists("auto_connect_count")) {
        config_["auto_connect_count"] = 0;
    }
    /*
     * The Syntax for adding new clients is:
     *  address_ + i         -> i is an iterator
     *  port + i
     *  Dont forget too increase "auto_connect_count" by one
     */
}

internal_essential::NetworkConfig::~NetworkConfig() = default;
