//
// Created by revol-xut on 11/1/19.
//

#ifndef WORKER_CONFIG_HPP
#define WORKER_CONFIG_HPP

#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <nlohmann/json.hpp>

//#include "../runtime/runtime.hpp"

/*
 *  The Config format is similar to .ini or .conf files
 *  [kHeader]
 *  key=value
 *  "#" for comments
 */

namespace internal_essential {

    class Config {
    public:
        explicit Config();

        /*!
         * @brief Creates Config Object from a path t the config file
         * @param path config file
         */
        explicit Config(const std::string &path);

        /*!
         * @brief Creates Config Object form path with the difference that you can disable the loading of the default values
         * @param path config file
         * @param load disables / enables load of default values
         */
        explicit Config(const std::string &path, bool load);

        virtual ~Config();

        /*!
         * @brief Reads Contents of given file and updates / overwrites internal default config
         * @param file path
         */
        void readFile(const std::string &file);

        /*!
         * @brief Returns config string based on given key
         * @param key Config Key
         * @return Config Value
         */
        auto getString(const std::string &key) -> std::string;

        /*!
         * @brief Returns config tries to decode findings into an integer
         * @param key Config Key
         * @return int based on key
         */
        auto getInteger(const std::string &key) -> int;

        /*!
         * @brief Tries to cast the config string value into a boolean value recognised values are: (True, true, 1, False, false, 0)\
         * If the cast failed the return value is false
         * @param key Config key value
         * @return boolean value
         */
        auto getBool(const std::string &key) -> bool;

        /*!
         * @brief Reloads config
         */
        void reload();

        /*!
         * @brief Sets new Config path.
         * @param path New config path
         */
        void setConfig(const std::string &path) {
            config_path_ = path;
        }

        /*!
         * @brief Returns path to config
         * @return config path
         */
        auto getPath() -> std::string {
            return config_path_;
        }

        /*!
         * @brief Sets value pair
         * @param key
         * @param value
         */
        void setValueString(const std::string &key, const std::string &value);

        /*!
         * @brief Checks if key exists
         * @param key
         * @return
         */
        auto keyExists(const std::string &key) -> bool;

        /*!
         * @brief Sets all default values
         */
        virtual void setDefaultValues();

    protected:
        // Config
        //std::map<std::string, std::string> config_;
        nlohmann::json config_;


    private:
        std::string config_path_;


    };
} // namespace internal_essential


#endif //WORKER_CONFIG_HPP
