//
// Created by einspaten on 14.04.20.
//

#ifndef HUNGARIANALGO_BASE64_HPP
#define HUNGARIANALGO_BASE64_HPP

#include <string>

// Credit to: https://rosettacode.org/wiki/Base64_decode_data#C.2B.2B with
// slight optical changes and modernizations

class Base64 {
 public:
  /*!
   * @brief Turns given string into base64 encoded one
   * @param input raw string
   * @param result where the return value will be written on
   */
  static void encode(const std::string &input, std::string &result);
  /*!
   * @brief Turns base64 encoded string into its raw form
   * @param data base64 encoded string
   * @param result result where the return value will be written on
   */
  static void decode(const std::string &data, std::string &result);

 private:
  static auto find_index(unsigned char byte) -> int;
  static std::string char_set;
};

#endif  // HUNGARIANALGO_BASE64_HPP
