//
// Created by einspaten on 14.04.20.
//

#include "base64.hpp"

#include <fstream>
#include <iostream>
#include <vector>

const unsigned int m1 = 63 << 18, m2 = 63 << 12, m3 = 63 << 6;

std::string Base64::char_set =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void Base64::encode(const std::string &input, std::string &result) {
  result.reserve(((input.size() / 3) + (input.size() % 3 > 0)) * 4);

  std::uint32_t temp{};
  auto it = input.begin();

  for (std::size_t i = 0; i < input.size() / 3; ++i) {
    temp = (*it++) << 16;
    temp += (*it++) << 8;
    temp += (*it++);
    result.push_back(char_set[(temp & 0x00FC0000) >> 18]);
    result.push_back(char_set[(temp & 0x0003F000) >> 12]);
    result.push_back(char_set[(temp & 0x00000FC0) >> 6]);
    result.push_back(char_set[(temp & 0x0000003F)]);
  }

  switch (input.size() % 3) {
    case 1:
      temp = (*it++) << 16;
      result.push_back(char_set[(temp & 0x00FC0000) >> 18]);
      result.push_back(char_set[(temp & 0x0003F000) >> 12]);
      result.push_back('=');
      result.push_back('=');
      break;
    case 2:
      temp = (*it++) << 16;
      temp += (*it++) << 8;
      result.push_back(char_set[(temp & 0x00FC0000) >> 18]);
      result.push_back(char_set[(temp & 0x0003F000) >> 12]);
      result.push_back(char_set[(temp & 0x00000FC0) >> 6]);
      result.push_back('=');
      break;
  }
}

auto Base64::find_index(unsigned char byte) -> int {
  return char_set.find(byte);
}

void Base64::decode(const std::string &data, std::string &result) {
  if (data.size() % 4 != 0) {
    throw new std::runtime_error("Error in size to the decode method");
  }

  auto it = data.cbegin();
  auto end = data.cend();

  while (it != end) {
    auto b1 = *it++;
    auto b2 = *it++;
    auto b3 = *it++;  // might be first padding byte
    auto b4 = *it++;  // might be first or second padding byte

    auto i1 = find_index(b1);
    auto i2 = find_index(b2);
    int acc;

    acc = i1 << 2;   // six bits came from the first byte
    acc |= i2 >> 4;  // two bits came from the first byte

    result.push_back(acc);  // output the first byte

    if (b3 != '=') {
      auto i3 = find_index(b3);

      acc = (i2 & 0xF) << 4;  // four bits came from the second byte
      acc |= i3 >> 2;         // four bits came from the second byte

      result.push_back(acc);  // output the second byte

      if (b4 != '=') {
        auto i4 = find_index(b4);

        acc = (i3 & 0x3) << 6;  // two bits came from the third byte
        acc |= i4;              // six bits came from the third byte

        result.push_back(acc);  // output the third byte
      }
    }
  }
}