//
// Created by revol-xut on 10/31/19.
//

#include <csignal>
#include <cstdlib>
#include <syslog.h>

#include "config/config.hpp"
#include "interface/network_interface.hpp"
#include "logging/signal_handler.hpp"
#include "network/network_head.hpp"
#include "runtime/application_constructor.hpp"


void deleteMessageStack(const std::shared_ptr<MessageList> &message_list) {
  for (const std::shared_ptr<RawMessage> &message : message_list->messages) {
    delete message->message;
  }
  message_list->messages.clear();
    message_list->message_count = 0;
}

auto main(int argc, char **argv) -> int {
    // SkeletonDaemon();
    signal(SIGCHLD, signalHandler);
    signal(SIGHUP, signalHandler);
    signal(SIGPIPE, signalHandler);
    signal(SIGABRT, signalHandler);

    ApplicationConstructor app;

    if (argc > 1) {
      std::string run_in_userspace = argv[1];
      if (run_in_userspace == "--user") {
        internal_essential::InformationObject::getRuntime()
            ->getNetworkConfig()
            ->setValueString("ipc", "/run/user/1000/russel.sock");
      }
    }

    NetworkHead network_head;
    NetworkInterface network_interface;
    auto messages_receive = std::make_shared<MessageList>();
    auto messages_respond = std::make_shared<MessageList>();

    while (internal_essential::InformationObject::getRuntime()->getRunning()) {
      network_head.checkEveryThing(messages_receive);
      NetworkInterface::executeStack(messages_receive, messages_respond);
      network_head.send(messages_respond);
      deleteMessageStack(messages_receive);  // deletes Messages

      messages_respond->message_count = 0;
      messages_respond->messages.clear();
      app.send_heart_beat();
      app.collect_garbage();
    }

    syslog(LOG_NOTICE, "Russel daemon terminated.");
    closelog();

    return EXIT_SUCCESS;
}
