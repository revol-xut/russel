//
// Created by revol-xut on 17.02.20.
//

#include "routine.hpp"

#include <fstream>
#include <iostream>

#include "../other/base64.hpp"

Routine::Routine() = default;

Routine::Routine(const std::shared_ptr<char[]> &data, unsigned int size) {
  this_routine_ = std::make_unique<runtime::Routine>(data, size);
}

Routine::~Routine() = default;

void Routine::load(const std::string &path) {
  std::ifstream file(path);

  file.seekg(0, std::ios::end);
  size_t length = file.tellg();
  file.seekg(0, std::ios::beg);

  auto data = new char[length + 1];  // + 1 because there will be \0 byte added
  file.read(data, length);

  if (file.failbit) {
    // Maybe throw an exception or read more data here
  }

  file.close();

  this_routine_ =
      std::make_shared<runtime::Routine>(std::shared_ptr<char[]>(data), length);

  routine_path_ = path;
}

void Routine::save(const std::string &path) {
  std::ofstream file(
      path, std::ofstream::out |
                std::ofstream::trunc);  // Wipes file and writes contents
  file.write(this_routine_->getData().get(), this_routine_->getSize());
  file.close();
}

void Routine::save_string(const std::string &path, const std::string &data) {
  std::ofstream file(
      path, std::ofstream::out |
                std::ofstream::trunc);  // Wipes file and writes contents
  file.write(data.c_str(), data.size());
  file.close();
}

auto Routine::serialize() -> nlohmann::json {
  nlohmann::json return_value;

  std::string raw_data =
      std::string(this_routine_->getData().get(), this_routine_->getSize());
  std::string encoded_data;

  Base64::encode(raw_data, encoded_data);
  // return_value["name"] = ; TODO: Required Name
  return_value["data"] = encoded_data;
  return_value["size"] = this_routine_->getSize();

  return return_value;
}

auto Routine::getRoutine() -> std::shared_ptr<runtime::Routine> {
  return this_routine_;
}

auto Routine::get_routine_path() -> std::string {
    return routine_path_;
}