//
// Created by revol-xut on 17.02.20.
//

#ifndef RUSSEL_ROUTINE_HPP
#define RUSSEL_ROUTINE_HPP

#include <nlohmann/json.hpp>
#include <russel-interpreter/routine/routine.hpp>

class Routine {
public:
    Routine();

    Routine(const std::shared_ptr<char[]> &data, unsigned int size);

    ~Routine();

    void save(const std::string &path);

    void load(const std::string &path);

    auto getRoutine() -> std::shared_ptr<runtime::Routine>;

    auto serialize() -> nlohmann::json;

    void static save_string(const std::string &path, const std::string &data);

    auto get_routine_path() -> std::string;
private:
    std::shared_ptr<runtime::Routine> this_routine_;
    std::string routine_path_;

};


#endif //RUSSEL_ROUTINE_HPP
