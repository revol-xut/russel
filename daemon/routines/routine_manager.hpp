//
// Created by revol-xut on 17.02.20.
//

#ifndef RUSSEL_ROUTINE_MANAGER_HPP
#define RUSSEL_ROUTINE_MANAGER_HPP

#include <map>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>

#include "../information_object.hpp"
#include "routine.hpp"

using json = nlohmann::json;

constexpr unsigned int kRoutineManager = 15;

class RoutineManager : public internal_essential::InformationObject {
public:
    RoutineManager();

    ~RoutineManager();

    /*!
     * @brief Loads Routine into given object
     * @param name  name of routine
     * @param ptr initialized object
     */
    void load_routine(const std::string &name, std::shared_ptr<Routine> &ptr);

    /*!
     * @brief Saves given routine and writes it to the disk
     * @param name name that the routine should get
     * @param ptr object
     */
    void save_routine(const std::string &name, std::shared_ptr<Routine> &ptr);

    void autoRespond(const std::shared_ptr<json> &input_json, const std::shared_ptr<json> &output_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

private:
    std::string base_path_;

};


#endif //RUSSEL_ROUTINE_MANAGER_HPP
