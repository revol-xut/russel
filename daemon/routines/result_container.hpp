//
// Created by revol-xut on 22.02.20.
//

#ifndef RUSSEL_RESULT_CONTAINER_HPP
#define RUSSEL_RESULT_CONTAINER_HPP

#include <russel-interpreter/data_classes/data_class_interface.hpp>
#include <russel-interpreter/data_classes/abstract_type.hpp>

class ResultContainer {
public:
    ResultContainer();

    ~ResultContainer();

    void parseFrom();


private:

    std::vector<datatypes::AbstractType> variables_ = {};


};


#endif //RUSSEL_RESULT_CONTAINER_HPP
