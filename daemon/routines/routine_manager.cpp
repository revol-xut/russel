//
// Created by revol-xut on 17.02.20.
//

#include "routine_manager.hpp"

#include <experimental/filesystem>

#include "../other/base64.hpp"

RoutineManager::RoutineManager() {
    base_path_ = runtime_->getConfig()->getString("routine_path");

    if (base_path_.back() != *"/") {
        base_path_ += "/";
    }

    addToRuntime();
}

RoutineManager::~RoutineManager() = default;

void RoutineManager::load_routine(const std::string &name, std::shared_ptr<Routine> &ptr) {
    ptr->load(base_path_ + name);
}

void RoutineManager::save_routine(const std::string &name, std::shared_ptr<Routine> &ptr) {
    ptr->save(base_path_ + name);
}

void RoutineManager::autoRespond(const std::shared_ptr<json> &input_json, const std::shared_ptr<json> &output_json) {

    std::string command = input_json->at("command");

    if (command == "save_and_add") {
        /*!
         * Uploads routine too engine
         */
        std::string name = input_json->at("name");

        bool force = false;

        if (input_json->find("force") != input_json->end()) {
          force = input_json->at("force");
        }

        if (std::experimental::filesystem::exists(base_path_ + name) and
            not force) {
          (*output_json)["error"] = "file_already_exists";
          return;
        }
        unsigned int size = input_json->at("size");
        auto program = std::make_shared<char[]>(size);
        auto raw_data = input_json->at("data").get<std::string>();
        std::string decoded = "";

        Base64::decode(raw_data, decoded);
        Routine::save_string(base_path_ + name, decoded);

        (*output_json)["success"] = true;

    } else if (command == "list") {
        std::vector<std::string> files = {};

        for (const auto &entry : std::experimental::filesystem::directory_iterator(base_path_)) {
            files.push_back(entry.path().filename());
        }
        (*output_json)["names"] = files;

    } else if (command == "remove") {
        std::string name = input_json->at("name");
        if (std::experimental::filesystem::exists(base_path_ + name)) {
            std::experimental::filesystem::remove(base_path_ + name);
            (*output_json)["success"] = "true";
        } else {
            (*output_json)["error"] = "file_not_found";
        }
    } else if (command == "request_routine") {
        // Reads routine and sends it
        if (input_json->find("name") == input_json->end()) {
            (*output_json)["error"] = "no_name_specified";
            return;
        }
        std::string name = input_json->at("name");

        if (not std::experimental::filesystem::exists(base_path_ + name)) {
            (*output_json)["error"] = "there_is_no_routine_with_given_name";
            return;
        }

        auto ptr = std::make_unique<Routine>();
        ptr->load(base_path_ + name);

        (*output_json)["routine"] = ptr->serialize();
    }
}

auto RoutineManager::hash() -> unsigned int {
    return kRoutineManager;
}

void RoutineManager::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<RoutineManager>(this);
    }
}