//
// Created by revol-xut on 07.04.20.
//

#ifndef RUSSEL_WORKLOAD_ESTIMATOR_HPP
#define RUSSEL_WORKLOAD_ESTIMATOR_HPP

#include <memory>
#include <map>
#include <russel-interpreter/routine/routine.hpp>
#include <stdint.h>

#include "language_maps.hpp"


class WorkloadEstimator {
    // This generates information should be saved with the given routine
public:
    WorkloadEstimator();
    WorkloadEstimator(const std::shared_ptr<char[]> &data, unsigned int size);
    explicit WorkloadEstimator(const std::shared_ptr<runtime::Routine> &routine);

    ~WorkloadEstimator();

    void generate_ratings();
    auto get_transmission_size() -> unsigned int;
    auto get_estimated_workload() -> unsigned int;

private:

    std::shared_ptr<runtime::Routine> routine_;

    unsigned int transmission_size_;
    unsigned int estimated_workload_;

};


#endif //RUSSEL_WORKLOAD_ESTIMATOR_HPP
