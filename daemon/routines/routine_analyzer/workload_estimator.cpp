//
// Created by revol-xut on 07.04.20.
//

#include "workload_estimator.hpp"

WorkloadEstimator::WorkloadEstimator() {
    routine_ = nullptr;
    transmission_size_ = 0;
    estimated_workload_ = 0;
}

WorkloadEstimator::WorkloadEstimator(const std::shared_ptr<runtime::Routine> &routine) {
    routine_ = routine;
    transmission_size_ = 0;
    estimated_workload_ = 0;
}

WorkloadEstimator::WorkloadEstimator(const std::shared_ptr<char[]> &data, unsigned int size) {
    routine_ = std::make_shared<runtime::Routine>(data, size);
    transmission_size_ = 0;
    estimated_workload_ = 0;
}

WorkloadEstimator::~WorkloadEstimator() = default;

void WorkloadEstimator::generate_ratings() {
  auto command = std::make_shared<runtime::Command>();
  Workload wl{};

  // Makes sure that the routine starts reading from position 0
  routine_->setPointer(0);
  routine_->set_done_false();

  while (not routine_->getDone()) {
    routine_->next(command);

    wl = EstimationStruct::estimations.at(command->command);
    transmission_size_ += wl.transmission_size;
    estimated_workload_ += wl.estimated_workload;

    command->jump = JumpSizes::jump_sizes.at(command->command);
  }
}

auto WorkloadEstimator::get_transmission_size() -> unsigned int {
    return transmission_size_;
}

auto WorkloadEstimator::get_estimated_workload() -> unsigned int {
    return estimated_workload_;
}