//
// Created by einspaten on 16.04.20.
//

#include "language_maps.hpp"

std::map<char, Workload> EstimationStruct::create_map() {
  std::map<char, Workload> m;
  m[0xC] = Workload{1, 1};    // Dec Float
  m[0xD] = Workload{10, 1};   // Dec Vec
  m[0xE] = Workload{100, 1};  // Dec Matrix
  m[0xF] = Workload{1, 1};    // Dec Boolean
  m[0x14] = Workload{0, 30};  // Add
  m[0x16] = Workload{0, 30};  // Mul
  m[0x1E] = Workload{0, 10};  // Jmp
  m[0x1F] = Workload{0, 5};   // If
  m[0x50] = Workload{0, 10};  // Equals
  m[0x51] = Workload{0, 10};  // Negate
  return m;
}

const std::map<char, Workload> EstimationStruct::estimations =
    EstimationStruct::create_map();

std::map<char, uint8_t> JumpSizes::create_map() {
  std::map<char, uint8_t> m;

  m[0xA] = 1;
  m[0xB] = 1;
  m[0xC] = 1;   // Dec Float
  m[0xD] = 2;   // Dec Vec
  m[0xE] = 3;   // Dec Mat2D
  m[0xF] = 1;   // Dec Bool
  m[0x14] = 4;  // Add
  m[0x15] = 4;  // Min
  m[0x16] = 4;  // Mul
  m[0x17] = 1;  // Pot (not implemented)
  m[0x1E] = 2;  // jmp
  m[0x28] = 2;  // write stdout
  m[0x50] = 4;  // Equals
  m[0x51] = 2;  // Boolean Negate
  m[0x1F] = 3;  // if
  m[0x33] = 1;  // Terminate
  m[0x32] = 2;  // Del

  return m;
}

const std::map<char, uint8_t> JumpSizes::jump_sizes = JumpSizes::create_map();