//
// Created by einspaten on 21.05.20.
//

#ifndef RUSSEL_SIZE_CALCULATOR_HPP
#define RUSSEL_SIZE_CALCULATOR_HPP

#include <map>
#include <russel-interpreter/routine/routine.hpp>
#include <vector>
#include <stdint.h>

struct VariableInformation {
  unsigned int size;
  unsigned int memory_position;
  unsigned int variable_value;  // Only set for scalar values
};

class SizeCalculator {
 public:
  SizeCalculator(const std::shared_ptr<runtime::Routine>& routine,
                 const std::string& data);
  SizeCalculator(const std::shared_ptr<runtime::Routine>& routine,
                 const std::shared_ptr<char[]>& data);
  ~SizeCalculator();

  void generate();

  auto get_variable_size(unsigned int index) -> unsigned int;
  auto get_variable_memory_pos(unsigned int index) -> unsigned int;
  auto get_variable(unsigned int index) -> VariableInformation;
  auto get_variable_count() const -> unsigned int;
  auto get_data_field_size() const -> unsigned int;

 private:
  std::shared_ptr<runtime::Routine> routine_;
  std::shared_ptr<char[]> data_;

  std::map<unsigned int, VariableInformation> variable_;

  unsigned int variable_counter_;
  unsigned int current_memory_offset_;
};

#endif  // RUSSEL_SIZE_CALCULATOR_HPP
