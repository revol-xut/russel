//
// Created by einspaten on 21.05.20.
//

#include "size_calculator.hpp"

#include <cmath>
#include <cstring>
#include <iostream>
#include <russel-interpreter/data_classes/data_class_interface.hpp>

#include "language_maps.hpp"

SizeCalculator::SizeCalculator(const std::shared_ptr<runtime::Routine> &routine,
                               const std::shared_ptr<char[]> &data) {
  routine_ = routine;
  data_ = data;
  variable_counter_ = 0;
  current_memory_offset_ = 0;
}

SizeCalculator::SizeCalculator(const std::shared_ptr<runtime::Routine> &routine,
                               const std::string &data) {
  routine_ = routine;
  variable_counter_ = 0;
  current_memory_offset_ = 0;

  auto temporary = new char[data.size()];
  std::memcpy(temporary, data.c_str(), data.size());

  data_ = std::shared_ptr<char[]>(temporary);
}

SizeCalculator::~SizeCalculator() { variable_.clear(); };

auto SizeCalculator::get_variable_size(unsigned int index) -> unsigned int {
  return variable_[index].size;
}

auto SizeCalculator::get_variable_memory_pos(unsigned int index)
    -> unsigned int {
  return variable_[index].memory_position;
}

auto SizeCalculator::get_variable(unsigned int index) -> VariableInformation {
  return variable_[index];
}

auto SizeCalculator::get_variable_count() const -> unsigned int {
  return variable_counter_;
}

auto SizeCalculator::get_data_field_size() const -> unsigned int {
  return current_memory_offset_;
}

void SizeCalculator::generate() {
  auto command = std::make_shared<runtime::Command>();
  command->jump = 0;

  // Makes sure that the routine starts reading from position 0
  routine_->setPointer(0);
  routine_->set_done_false();

  unsigned current_pos = 0;
  while (current_pos < routine_->getSize()) {
    routine_->next(command);
    if (command->command == declare_float) {  // Declares float
      VariableInformation var{};
      var.size = 4;
      var.memory_position = current_memory_offset_;
      var.variable_value = round(datatypes::castToValue(
          data_.get() + current_memory_offset_));  // NOLINT

      variable_[variable_counter_] = var;
      current_memory_offset_ += 4;
      variable_counter_++;
    } else if (command->command == declare_vec) {  // declare vector

      unsigned int vector_size = variable_[command->arg1].variable_value;

      VariableInformation var{};
      var.size = vector_size;
      var.memory_position = current_memory_offset_;

      variable_[variable_counter_] = var;
      current_memory_offset_ += vector_size * sizeof(float);
      variable_counter_++;
    } else if (command->command == declare_mat) {  // declare Matrix

      unsigned int column_size = variable_[command->arg1].variable_value;
      unsigned int row_size = variable_[command->arg2].variable_value;

      VariableInformation var{};
      var.size = column_size * row_size;
      var.memory_position = current_memory_offset_;

      variable_[variable_counter_] = var;
      current_memory_offset_ += var.size * sizeof(float);
      variable_counter_++;
    }
    if (JumpSizes::jump_sizes.find(command->command) ==
        JumpSizes::jump_sizes.end()) {
      return;
    }
    command->jump = JumpSizes::jump_sizes.at(command->command);
    current_pos += JumpSizes::jump_sizes.at(command->command);
  }
}