//
// Created by revol-xut on 08.04.20.
//

#ifndef RUSSEL_LANGUAGE_MAPS_HPP
#define RUSSEL_LANGUAGE_MAPS_HPP

#include <map>
#include <stdint.h>

constexpr char declare_float = 0xC;
constexpr char declare_vec = 0xD;
constexpr char declare_mat = 0xE;

struct Workload {
  unsigned int transmission_size;
  unsigned int estimated_workload;
};

struct EstimationStruct {
  static std::map<char, Workload> create_map();

  static const std::map<char, Workload> estimations;
};

struct JumpSizes {
  static std::map<char, uint8_t> create_map();

  static const std::map<char, uint8_t> jump_sizes;
};


#endif //RUSSEL_LANGUAGE_MAPS_HPP
