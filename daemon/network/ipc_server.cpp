//
// Created by revol-xut on 11/9/19.
//

#include "ipc_server.hpp"

IPCServer::IPCServer(const std::string &sock_path) {

    if ((this_socket_ = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(ERROR) << "ipc SocketInterface could not be created";
            runtime_->terminate();
        }
        return;
    }

    memset(&address_, 0, sizeof(address_));
    address_.sun_family = AF_UNIX;
    strncpy(static_cast<char *>(address_.sun_path), sock_path.c_str(), sizeof(address_.sun_path) - 1);
    unlink(sock_path.c_str());
    bindToAddr();
    addToRuntime();
}

IPCServer::IPCServer() {

    if (runtime_ != nullptr) {
        unix_domain_socket_ = runtime_->getNetworkConfig()->getString("ipc");
    } else {
        unix_domain_socket_ = "/etc/russel/russel.sock";
    }

    if ((this_socket_ = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(ERROR) << "ipc SocketInterface could not be created";
            runtime_->terminate();
        }
        return;
    }
    memset(&address_, 0, sizeof(address_));
    address_.sun_family = AF_UNIX;

    strncpy(static_cast<char *>(address_.sun_path), unix_domain_socket_.c_str(), sizeof(address_.sun_path) - 1);
    unlink(unix_domain_socket_.c_str());
    bindToAddr();
    addToRuntime();

}

IPCServer::~IPCServer() = default;

void IPCServer::listenConnection() {
    if (listen(this_socket_, kConnectionIpcCache) < 0) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "Listening on kIpc Socket failed";
        }
    }
}

auto IPCServer::acceptConnection() -> std::shared_ptr<Connection> {

    socklen_t address_length = sizeof(struct sockaddr_in);
    char str[INET_ADDRSTRLEN];

    int new_socket = accept(this_socket_, (struct sockaddr *) &address_, &address_length); //NOLINT C-Cast is necessary
    if (new_socket < 0 && runtime_ != nullptr) {
        runtime_->getLogger(ERROR) << "invalid ipc connection";
    }

    sockets_.push_back(new_socket);
    valid_.insert(std::pair<int, bool>(new_socket, true));
    inet_ntop(AF_INET, &(address_), static_cast<char *>(str), INET_ADDRSTRLEN);
    auto connection = std::make_shared<Connection>();

    connection->socket = new_socket;
    connection->address = std::string(static_cast<char *>(str), INET_ADDRSTRLEN);

    return connection;
}

auto IPCServer::bindToAddr() -> int {
    if (bind(this_socket_,
             (struct sockaddr *) &address_, //NOLINT C-Cast is necessary
             sizeof(address_)) == -1) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(ERROR) << "kIpc SocketInterface could not be bind too this address_";
        }

        return 1;
    }
    return 0;
}

void IPCServer::autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                            const std::shared_ptr<internal_essential::json> &return_json) {
    if (json_data->at("command") == "ipc_socket") {
        (*return_json)["data"] = {{"ipc_socket", unix_domain_socket_}};
    }
}

auto IPCServer::hash() -> unsigned int {
    return 5;
}

void IPCServer::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<IPCServer>(this);
    }
}
