//
// Created by revol-xut on 12/8/19.
//

#include "network_head.hpp"

NetworkHead::NetworkHead() {
  addToRuntime();
  ipc_ = true, pub_ = true, cli_ = true;

  if (runtime_ != nullptr) {
    ipc_ = runtime_->getNetworkConfig()->getBool("enable_ipc");
    pub_ = runtime_->getNetworkConfig()->getBool("enable_pub");
    cli_ = runtime_->getNetworkConfig()->getBool("enable_cli");
  }

  if (ipc_) {
    server_ipc_ = std::make_shared<ServerContainer>(true);
  }
  if (pub_) {
    server_pub_ = std::make_shared<ServerContainer>(false);
  }
  if (cli_) {
    client_pub_ = std::make_shared<ClientContainer>();
    autoConnect();
  }

  thread_ = std::make_unique<std::thread>(&NetworkHead::send_back_loop, this);
  send_back_loop_running_ = true;

  nlohmann::json data_json = {
      {"command", "api"},
      {"id", 22},
      {"data",
       {{"command", "register"}, {"is_engine", true}, {"respond", true}}}};
  data_ = data_json.dump();
}

NetworkHead::NetworkHead(bool ipc, bool pub, bool clients) {
  this->ipc_ = ipc, this->pub_ = pub, this->cli_ = clients;
  if (ipc) {
    server_ipc_ = std::make_shared<ServerContainer>(true);
  }
  if (pub) {
    server_pub_ = std::make_shared<ServerContainer>(false);
  }
  if (clients) {
    client_pub_ = std::make_shared<ClientContainer>();
    autoConnect();
  }
  nlohmann::json data_json = {
      {"command", "api"},
      {"id", 22},
      {"data",
       {{"command", "register"}, {"is_engine", true}, {"respond", true}}}};
  data_ = data_json.dump();
}

NetworkHead::~NetworkHead() {
  send_back_loop_running_ = false;
  mutex_.unlock();
  cv_.notify_all();
  if (thread_ != nullptr) {
    thread_->join();

    delete thread_.get();
  }
}

void NetworkHead::checkEveryThing(
    const std::shared_ptr<MessageList> &messages) {
  if (ipc_) {
    server_ipc_->checkConnections();
    server_ipc_->checkReceiving(messages);
  }
  if (pub_) {
    server_pub_->checkConnections();
    server_pub_->checkReceiving(messages);
  }
  if (cli_) {
    client_pub_->checkReceiving(messages);
  }
}

void NetworkHead::autoConnect() {
  if (!cli_) {
    return;
  }

  int client_count = 0;

  if (runtime_ != nullptr) {
    client_count =
        runtime_->getNetworkConfig()->getInteger("auto_connect_count");
  }

  auto message = std::make_shared<RawMessage>();
  message->message = data_.c_str();
  message->size = data_.size();

  std::string address;
  unsigned short port = 0;
  for (int i = 0; i < client_count; i++) {
    if (not runtime_->getNetworkConfig()->keyExists("host_" +
                                                    std::to_string(i)) or
        not runtime_->getNetworkConfig()->keyExists("port_" +
                                                    std::to_string(i))) {
      runtime_->getLogger(ERROR)
          << "Invalid Network Config ... Auto-connect failed";
    }

    address =
        runtime_->getNetworkConfig()->getString("host_" + std::to_string(i));
    port = static_cast<unsigned short>(
        runtime_->getNetworkConfig()->getInteger("port_" + std::to_string(i)));

    auto sock = client_pub_->connectToEngine(address, port);

    if (sock > 0) {
      runtime_->getLogger(DEBUG)
          << "Could not connect to " + address + ":" + std::to_string(port);
      message->socket = sock;

      // Makes sure that the connection is established TODO: remove later
      std::this_thread::sleep_for(std::chrono::milliseconds(kWaitTime));
      client_pub_->send_message(message);
    }
  }
}

void NetworkHead::send(const std::shared_ptr<MessageList> &messages) {
  for (const std::shared_ptr<RawMessage> &temp_message : messages->messages) {
    merge_send_back(temp_message);
  }

  additional_tasks_ = 0;
  while (static_cast<int>(queue_.size()) - static_cast<int>(additional_tasks_) >
         0) {
    std::this_thread::sleep_for(std::chrono::milliseconds(
        kWaitTime));  // Makes sure that every message is sent
  }
}

void NetworkHead::send_single_message(
    const std::shared_ptr<RawMessage> &message) {
  switch (message->origin) {
    case 0:
      server_ipc_->routeAndSend(message);
      break;
    case 1:
      server_pub_->routeAndSend(message);
      break;
    case 2:
      client_pub_->send_message(message);
      break;
    default:
      if (runtime_ != nullptr) {
        runtime_->getLogger(ERROR)
            << "origin flag on RawMessage container was unknown";
      }
      break;
  }
}

void NetworkHead::merge_send_back(
    const std::shared_ptr<RawMessage> &raw_message) {
  queue_.push(raw_message);

  mutex_.unlock();
  cv_.notify_all();
}

void NetworkHead::send_back_loop() {
  while (send_back_loop_running_) {
    std::unique_lock<std::mutex> lock(mutex_);

    while (not queue_.empty()) {
      try {
        auto message = queue_.front();
        if (message != nullptr and message->message != nullptr and
            message->size > 0) {
          send_single_message(message);
          delete message->message;
        } else {
          break;
        }
      } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
      }

      queue_.pop();
    }
    cv_.wait(lock);
  }
}

void NetworkHead::autoRespond(
    const std::shared_ptr<internal_essential::json> &json_data,
    const std::shared_ptr<internal_essential::json> &return_json) {

  std::string command = json_data->at("command");
  if (command == "active") {
    (*return_json)["data"] = {{"ipc", ipc_}, {"pub", pub_}, {"cli", cli_}};
  } else if (command == "send_message") {
    auto raw_message = std::make_shared<RawMessage>();
    raw_message->socket = json_data->at("socket");
    raw_message->origin = json_data->at("origin");
    std::string data = json_data->at("data");

    raw_message->size = data.size();

    auto data_ptr = new char[raw_message->size];
    std::memcpy(data_ptr, data.c_str(), raw_message->size);
    raw_message->message = data_ptr;

    merge_send_back(raw_message);
    additional_tasks_++;
  }else if ( command == "send_message_from_shared"){
      unsigned int id = json_data->at("id");
      auto data = runtime_->get_global_memory()->get_chunk_data(id);

      auto raw_message = std::make_shared<RawMessage>();
      raw_message->socket = json_data->at("socket");
      raw_message->origin = json_data->at("origin");

      raw_message->message = (char*)data.get(); //NOLINT

      merge_send_back(raw_message);
      additional_tasks_++;
  }
}

auto NetworkHead::hash() -> unsigned int { return kIDNetworkHead; }

void NetworkHead::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<NetworkHead>(this);
  }
}