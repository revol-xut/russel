//
// Created by revol-xut on 11/23/19.
//

#include "public_server.hpp"

PublicServer::PublicServer(const std::string &ip, unsigned short port) {
    host_ = ip;
    port_ = port;
    if (configure() != 0) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "Failed to setup public Socket";
        }
    }
    addToRuntime();
}

PublicServer::PublicServer() {

    if (runtime_ != nullptr) {
        host_ = runtime_->getNetworkConfig()->getString("host");
        port_ = static_cast<unsigned short>(runtime_->getNetworkConfig()->getInteger("port"));
    } else {
        host_ = "127.0.0.1";
        port_ = 8321;
    }

    if (configure() != 0) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "Failed to setup public Socket";
        }
    }
    addToRuntime();
}

PublicServer::~PublicServer() = default;

auto PublicServer::configure() -> int {
  if ((this_socket_ = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    if (runtime_ != nullptr) {
      runtime_->getLogger(FATAL)
          << "Public SocketInterface could not be created";
    }
    return 1;
  }

  std::cout << "Bind to " + host_ + ":" + std::to_string(port_) << std::endl;

  if (runtime_ != nullptr) {
    runtime_->getLogger(INFO)
        << "Bind to " + host_ + ":" + std::to_string(port_);
  }

  valid_ = {};
  sockets_ = {};

  address_.sin_family = AF_INET;
  inet_pton(AF_INET, host_.c_str(), &address_.sin_addr);
  address_.sin_port = htons(port_);

  if (bindToAddr() == 0 && runtime_ != nullptr) {
    runtime_->getLogger(INFO) << "Public Socket bind was successful";
  } else {
        return 1;
    }
    return 0;
}


auto PublicServer::bindToAddr() -> int {

    if (setsockopt(this_socket_, SOL_SOCKET,
                   SO_REUSEADDR | SO_REUSEPORT, &kOpt_,
                   sizeof(kOpt_))) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "setsockopt failed";
        }
        return 1;
    }

    if (bind(this_socket_, (struct sockaddr *) &address_, //NOLINT C-Cast is necessary
             sizeof(address_)) < 0) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "public server socket bind failed";
        }
        return 1;
    }
    return 0;
}

void PublicServer::listenConnection() {
    if (listen(this_socket_, kConnectionPubCache) < 0) {
        if (runtime_ != nullptr) {
            runtime_->getLogger(FATAL) << "Listening on public Socket failed";
        }
    }
}

auto PublicServer::acceptConnection() -> std::shared_ptr<Connection> {

    char str[INET_ADDRSTRLEN];
    int new_socket = 0;
    if ((new_socket = accept(
            this_socket_, (struct sockaddr *) &address_, (socklen_t *) &addrlen_)) < 0) { //NOLINT C-Cast is necessary
        std::cerr << "Accept on public Socket failed" << std::endl;
    }
    sockets_.push_back(new_socket);
    valid_.insert(std::pair<int, bool>(new_socket, true));

    inet_ntop(AF_INET, &(address_), static_cast<char *>(str), INET_ADDRSTRLEN);

    auto connection = std::make_shared<Connection>();

    connection->socket = new_socket;
    connection->address = std::string(static_cast<char *>(str), INET_ADDRSTRLEN);

    return connection;

}


void PublicServer::autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                               const std::shared_ptr<internal_essential::json> &return_json) {
    std::string command = json_data->at("command");
    if (command == "host") {
        (*return_json)["data"] = {{"host", host_}};
    } else if (json_data->at("command") == "host") {
        (*return_json)["data"] = {{"port", port_}};
    }
}

auto PublicServer::hash() -> unsigned int {
    return 6;
}

void PublicServer::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<PublicServer>(this);
    }
}