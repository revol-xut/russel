//
// Created by worker on 12/7/19.
//

#ifndef RUSSEL_CLIENT_HPP
#define RUSSEL_CLIENT_HPP

#include "socket.hpp"

class Client : public ClientSocket {
public:

    /*!
     * Creates Client object
     */
    explicit Client();

    /*!
     * Destructor
     */
    ~Client();

    /*!
     * @brief Will try to connect to given address will push information on provided raw pointer
     * @param host IP Address
     * @param port Port
     * @param con  Raw pointer too connection object
     * @return status 0 ok -1 failed
     */
    auto connectViaHost(const std::string &host, unsigned short port, Connection *con) -> int override;

    /*!
     * @brief Will try to connect to given address will push information on provided smart pointer
     * @param host IP Address
     * @param port Port
     * @param con Smart pointer too Connection object
     * @return status 0 ok -1 failed
     */
    auto connectViaHost(const std::string &host, unsigned short port,
                        const std::shared_ptr<Connection> &con) -> int override;

    auto bindToAddr() -> int override;

    void autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                     const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

private:

    struct sockaddr_in server_addr_{};

};

#endif //RUSSEL_CLIENT_HPP
