//
// Created by revol-xut on 11/9/19.
//
#include "socket.hpp"

#include <chrono>
#include <cmath>
#include <cstring>
#include <iostream>
#include <thread>

using uchar = unsigned char;

void createHeader(uint32_t size, char *return_value) {
  return_value[0] = *((uchar *)(&size));      // NOLINT
  return_value[1] = *((uchar *)(&size) + 1);  // NOLINT
  return_value[2] = *((uchar *)(&size) + 2);  // NOLINT
  return_value[3] = *((uchar *)(&size) + 3);  // NOLINT
}

auto unpackHeader(const char *data) -> uint32_t {
  uint32_t output;

  *((uchar *)(&output)) = (uchar)data[0];      // NOLINT
  *((uchar *)(&output) + 1) = (uchar)data[1];  // NOLINT
  *((uchar *)(&output) + 2) = (uchar)data[2];  // NOLINT
  *((uchar *)(&output) + 3) = (uchar)data[3];  // NOLINT

  return output;
}

auto SocketInterface::sendString(const std::string &message, int socket)
    -> int {
  auto *raw_message = new RawMessage{};

  raw_message->message = message.c_str();
  raw_message->size = message.size();
  raw_message->socket = socket;

  auto status = sendByte(raw_message);

  delete raw_message;
  return status;
}

auto SocketInterface::sendRawMessage(const RawMessage &message) -> int {
  return sendByte(&message);
}

auto SocketInterface::sendSmartPointer(
    const std::shared_ptr<RawMessage> &message) -> int {
  return sendByte(message.get());
}

auto SocketInterface::sendByte(const RawMessage *message) -> int {
  /* The Russel Daemon comes with his own transmission security it creates a 4
   * bytes big header which contains the size of the package.
   */
  size_t size = message->size;
  int socket = message->socket;
  const char *content = message->message;
  char *const header = new char[4];

  createHeader(static_cast<uint32_t>(size + 4), header);
  if (write(socket, header, 4) <= 0) {
    if (valid_.find(socket) != valid_.end()) {
      valid_.at(socket) = false;
    }
    delete[] header;
    return 1;
  }
  if (write(socket, content, size) <= 0) {
    if (valid_.find(socket) != valid_.end()) {
      valid_.at(socket) = false;
    }
    delete[] header;
    return 1;
  }

  // delete content;
  delete[] header;
  return 0;
}

void SocketInterface::receive(const std::shared_ptr<MessageList> &message_list,
                              int socket) {
  char *const temp_buffer = new char[kBufferSize];
  int bytes_readen =
      static_cast<size_t>(recv(socket, temp_buffer, kBufferSize, 0));

  if (bytes_readen <= 0) {
    valid_.at(socket) = false;  // check if connection is still valid_ SIGPIPE
    delete[] temp_buffer;
    return;
  }
  uint32_t expected_size;
  unsigned int current_pos = 0;
  bool break_loop = false;

  do {
    expected_size = unpackHeader(temp_buffer + current_pos);

    int single_message_size = expected_size - bytes_readen + current_pos;

    char *total = new char[expected_size - 4];
    unsigned timeout_timer = kTimeout;
    int i = current_pos;
    unsigned write_pos =
        std::min(expected_size, bytes_readen - current_pos) - 4;
    std::memcpy(total, temp_buffer + i + 4,
                std::min(expected_size, bytes_readen - current_pos) - 4);

    if (single_message_size > 0) {
      while (single_message_size > 0) {
        bytes_readen = static_cast<size_t>(
            read(socket, total + write_pos, single_message_size));
        if (bytes_readen <= 0) {
          valid_.at(socket) = false;
          delete[] temp_buffer;
          return;
        }

        single_message_size -= bytes_readen;
        write_pos += bytes_readen;
      }

      timeout_timer--;
      break_loop = true;
    }

    auto raw_message = std::make_shared<RawMessage>();
    raw_message->message = total;
    raw_message->size = expected_size - 4;
    raw_message->socket = socket;
    message_list->messages.push_back(raw_message);
    message_list->message_count++;
    current_pos += expected_size;
  } while (current_pos < static_cast<unsigned int>(bytes_readen) and
           not break_loop);
  delete[] temp_buffer;
}

void SocketInterface::closeSocket(int input_socket) {
  if (valid_.find(input_socket) != valid_.end() and valid_.at(input_socket)) {
    close(input_socket);
    valid_.erase(input_socket);
  }
}

auto SocketInterface::checkValidity(int socket) -> bool {
  if (valid_.find(socket) != valid_.end()) {
    return valid_.at(socket);
  }
  return false;
}

auto SocketInterface::getSocket() const -> int { return this_socket_; }

void SocketInterface::set_valid(int socket) { valid_[socket] = true; }