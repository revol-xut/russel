//
// Created by revol-xut on 11/23/19.
//

#include "socket_handler.hpp"

#include <functional>

void SocketWatcher::checkReceiving(const std::shared_ptr<MessageList> &ptr) {
    socket_read_ = socket_set_;
    unsigned i = 0;
    int rv = 0;

    for (const std::shared_ptr<Connection> &connection : connections_) {
      if (connection == nullptr) {
        continue;
      }

      if (getIdentity() == kCLIENT) {
        timeval timeout{0, kTimeout};
        rv = select(connection->socket, &socket_read_, NULL, NULL, &timeout);
      }

      if ((!connection->ignore and
           FD_ISSET(connection->socket, &socket_read_)) or
          rv > 0) {  // NOLINT
        unsigned int start_index = ptr->message_count;
        my_socket_->receive(ptr, connection->socket);

        for (unsigned int j = start_index; j < ptr->messages.size(); j++) {
          ptr->messages[j]->origin = identity_;
        }

        if (!my_socket_->checkValidity(connection->socket)) {
          FD_CLR(connection->socket,
                 &socket_set_);  // NOLINT deletes socket from set
          eraseSocket(i);        // deletes corresponding connection
          i--;                   // decreases iterator

          if (runtime_ != nullptr and
              objects_.find(22) !=
                  objects_.end()) {  // This now calls the OtherEngineController
                                     // and unregisters current connection
            runtime_->getLogger(INFO) << "Deleted Socket";
            auto input_json = std::make_shared<nlohmann::json>();
            auto output_json = std::make_shared<nlohmann::json>();

            (*input_json)["command"] = "unregister";
            (*input_json)["socket_id"] = connection->socket;
            (*input_json)["socket_origin"] = kCLIENT;
            auto return_json = std::make_shared<nlohmann::json>();
            objects_.at(22)->autoRespond(input_json, return_json);
          }
        }
      }
        i++;
    }
}

void SocketWatcher::eraseSocket(unsigned connection_index) {
    if (connection_index < connections_.size()) {
        my_socket_->closeSocket(connections_[connection_index]->socket);
        connections_.erase(connections_.begin() + connection_index);
    } else {
        if (runtime_ != nullptr) {
            runtime_->getLogger(ERROR) << "SocketHandler wanted to destroy an non existing socket";
        }
    }
}

void SocketWatcher::findAndErase(int socket) {
    unsigned int i = 0;
    for (const std::shared_ptr<Connection> &connection : connections_) {
        if (connection->socket == socket) {
            runtime_->getLogger(INFO) << "Deleted Socket";
            FD_CLR(connection->socket, &socket_set_); // NOLINT deletes socket from set
            eraseSocket(i); // deletes corresponding connection
            i--; //decreases iterator
        }
        i++;
    }
}

void SocketWatcher::routeAndSend(const std::shared_ptr<RawMessage> &message) {
  routeAndSend(message.get());
}

void SocketWatcher::routeAndSend(const RawMessage *message) {
  if (identity_ == kIpc or identity_ == kPUBLIC) {
    if (my_socket_->checkValidity(message->socket)) {
      my_socket_->sendRawMessage(*message);
    }
  } else {
    std::cerr << "Error" << std::endl;
  }
}

void SocketWatcher::appendConnection(const std::shared_ptr<Connection> &connection) {
    connections_.push_back(connection);
}

void SocketWatcher::setIdentity(unsigned short id) {
    identity_ = id;
}

auto SocketWatcher::getIdentity() -> unsigned short {
    return identity_;
}

auto SocketWatcher::getSocket() -> std::shared_ptr<SocketInterface> {
    return my_socket_;
}

void SocketWatcher::setSocket(const std::shared_ptr<SocketInterface> &ptr) {
    my_socket_ = ptr;
}

auto SocketWatcher::getConnectionCount() -> unsigned int {
    return connections_.size();
}