//
// Created by revol-xut on 11/23/19.
//

#ifndef RUSSEL_PUBLIC_SERVER_HPP
#define RUSSEL_PUBLIC_SERVER_HPP

#include "socket.hpp"

constexpr int kConnectionPubCache = 10;

class PublicServer : public ServerSocket {
public:
    /*!
     * @brief Uses default values from config ('host', 'port') to create the exposed server socket
     */
    PublicServer();

    /*!
     * @brief Uses given values to create the exposed server socket
     * @param ip Ipv4 Address
     * @param port Port
     */
    explicit PublicServer(const std::string &ip, unsigned short port);

    /*!
     * @brief Default destructor
     */
    ~PublicServer();

    /*!
     * @brief Listens for incoming connections and accepts them.
     * @return Returns struct with all relevant information
     */
    auto acceptConnection() -> std::shared_ptr<Connection> override;

    void listenConnection() override;

    void autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                     const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

private:
    /*!
     * @brief Bind Socket too defined values
     * @return success status 0 or failed 1
     */
    auto bindToAddr() -> int override;

    auto configure() -> int;


    const int kOpt_ = 1;
    struct sockaddr_in address_{};
    const int addrlen_ = sizeof(address_);

    std::string host_;
    unsigned short port_;

};


#endif //RUSSEL_PUBLIC_SERVER_HPP
