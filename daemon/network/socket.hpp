//
// Created by revol-xut on 11/9/19.
//

#ifndef WORKER_SOCKET_HPP
#define WORKER_SOCKET_HPP

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <memory>
#include <tuple>
#include <vector>

#include "../addresser/murmur_hash.hpp"
#include "../information_object.hpp"

constexpr int kBufferSize = 1024;
constexpr int kTimeout = 64;
constexpr unsigned kWaitTime = 1;  // in Milliseconds

/*!
 * @brief Creates message header which are the first 4 Bytes of a transmission
 * that tell how long the message is
 * @param size Size of the entire Message including the first 4 Bytes
 * @param return_value Message
 */
void createHeader(uint32_t size, char *return_value);

/*!
 * @brief Takes the first 4 Bytes of an transmission and casts in into the size
 * of the message
 * @param data Message
 * @return Size
 */
auto unpackHeader(const char *data) -> uint32_t;

/*!
 * @brief Structure in which Connection will be saved.
 */
struct Connection {
    bool ignore = false; // Used for transmission data in separate threads so that the main process does not interfere
    void *client_obj = nullptr; // Easy access when this is a connection too a other server will be casted into Client when used
    int socket = -1;
    std::string address = "";
};

/*!
 * @brief Message Container
 */
struct RawMessage {
  const char *message = nullptr;
  size_t size = 0;
  int socket = -1;
  unsigned short origin = 4;  // 0 -> ipc_, 1->pub, 2->client
};

struct MessageList {
  unsigned message_count;
  std::vector<std::shared_ptr<RawMessage>> messages;
};

class SocketInterface : public internal_essential::InformationObject {
 public:
  /*!
   * @brief Tries to receive and will return it as a std::string
   * @param sock socket
   * @return message
   */
  // auto receiveString(int sock) -> std::string;

  /*!
     * @brief Tries to receive and will push it on the message pointer
     * @param message smart pointer to a RawMessage struct
     * @param sock socket
     */
  // void receiveSmartPtrByte(const std::shared_ptr<RawMessage> &message, int
  // sock);

  /*!
   * @brief Tries to receive and will push it on the message pointer
   * @param message_list where the resulting messages will be put
   */
  void receive(const std::shared_ptr<MessageList> &message_list, int socket);

  /*!
     * @brief Tries to send the given string over given socket
     * @param message Message as std::string
     * @param socket socket
     * @return status
     */
    auto sendString(const std::string &message, int socket) -> int;

    /*!
     * @brief Tries to send given reference over given socket
     * @param message reference to RawMessage struct
     * @return status
     */
    auto sendRawMessage(const RawMessage &message) -> int;

    /*!
     * @brief Tries to send given pointer over given socket
     * @param message C-Pointer to RawMessage struct
     * @param socket socket
     * @return status
     */
    auto sendByte(const RawMessage *message) -> int;

    /*!
     * @brief Tries to send given smart pointer over given socket
     * @param message Smart Pointer to RawMessage struct
     * @return status
     */
    auto sendSmartPointer(const std::shared_ptr<RawMessage> &message) -> int;

    /*!
     * @brief Closes given socket connection
     * @param input_socket socket
     */
    void closeSocket(int input_socket);

    /*!
     * @brief Returns if the given socket connection is still valid
     * @param socket socket
     * @return valid
     */
    auto checkValidity(int socket) -> bool;

    /*!
     * @brief Returns this socket
     * @return socket
     */
    auto getSocket() const -> int;

    /*!
     *
     * @param socket
     */
    void set_valid(int socket);

   protected:
    virtual auto bindToAddr() -> int = 0;

    std::map<int, bool> valid_;
    std::vector<int> sockets_;
    int this_socket_;
};

class ServerSocket : public SocketInterface {
public:
    /*!
     * @brief Fully virtual method that will set the socket on listening mode
     */
    virtual void listenConnection() = 0;

    /*!
     * @brief Checks if there is a new Connection returns the container.
     * @return
     */
    virtual auto acceptConnection() -> std::shared_ptr<Connection> = 0;
};

class ClientSocket : public SocketInterface {
public:

    /*!
     * @brief Will try to connect to given address will push information on provided raw pointer
     * @param host IP Address
     * @param port Port
     * @param con  Raw pointer too connection object
     * @return status 0 ok -1 failed
     */
    virtual auto connectViaHost(const std::string &host, unsigned short port, Connection *con) -> int = 0;

    /*!
     * @brief Will try to connect to given address will push information on provided smart pointer
     * @param host IP Address
     * @param port Port
     * @param con Smart pointer too Connection object
     * @return status 0 ok -1 failed
     */
    virtual auto
    connectViaHost(const std::string &host, unsigned short port, const std::shared_ptr<Connection> &con) -> int = 0;

protected:

    std::string host_;
    unsigned short port_;

};


#endif //WORKER_SOCKET_HPP
