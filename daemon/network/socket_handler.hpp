//
// Created by revol-xut on 11/23/19.
//

#ifndef RUSSEL_SOCKET_HANDLER_HPP
#define RUSSEL_SOCKET_HANDLER_HPP

#include <memory>
#include <vector>

#include "ipc_server.hpp"
#include "public_server.hpp"
#include "socket.hpp"
#include "../information_object.hpp"

constexpr int kIpc = 0;
constexpr int kPUBLIC = 1;
constexpr int kCLIENT = 2;

class SocketWatcher : public internal_essential::InformationObject {
public:
    /*!
     * @brief Checks all registered sockets if the received something and closes invalid ones
     * @param ptr Pointer where the data should be put on
     */
    void checkReceiving(const std::shared_ptr<MessageList> &ptr);

    /*!
     * @brief Sends message too the client
     * @param message
     */
    void routeAndSend(const std::shared_ptr<RawMessage> &message);

    /*!
     * @brief Sends message too the client
     */
    void routeAndSend(const RawMessage *message);

    /*!
     * @brief Appends given connection to the connection list
     */
    void appendConnection(const std::shared_ptr<Connection> &connection);

    /*!
     * @brief Sets identity value which tell what kind of function this object currently provides (ipc, pub,client)
     */
    void setIdentity(unsigned short id);

    /*!
     * @brief Returns identity
     * @return identity
     */
    auto getIdentity() -> unsigned short;

    /*!
     * @brief Returns pointer to the handled socket
     * @return socket pointer
     */
    auto getSocket() -> std::shared_ptr<SocketInterface>;

    /*!
     * @brief Sets the socket which should be managed by this object
     * @param ptr
     */
    void setSocket(const std::shared_ptr<SocketInterface> &ptr);

    /*!
     * @brief Deletes socket
     * @param connection_index index in the client list
     */
    void eraseSocket(unsigned connection_index);

    /*!
     * @brief Resolves index of given socket and deletes it
     * @param socket
     */
    void findAndErase(int socket);

    /*!
     * @brief returns the number of connections this object is holding
     * @return connection_count
     */
    auto getConnectionCount() -> unsigned int;

private:

    std::shared_ptr<SocketInterface> my_socket_;
    std::vector<std::shared_ptr<Connection>> connections_; // All connected connections_ including other ipc_ sockets_

    unsigned short identity_;

protected:

    fd_set socket_set_, socket_read_; //NOLINT the two container classes need direct access

};


#endif //RUSSEL_SOCKET_HANDLER_HPP
