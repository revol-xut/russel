//
// Created by worker on 12/7/19.
//

#include "client.hpp"

Client::Client() {
    if ((this_socket_ = socket(AF_INET, SOCK_STREAM, 0)) < 0 && runtime_ != nullptr) {
        runtime_->getLogger(FATAL) << "Failed to setup client Socket";
    }
    server_addr_ = {};
    addToRuntime();
}

Client::~Client() = default;


auto
Client::connectViaHost(const std::string &host, unsigned short port, const std::shared_ptr<Connection> &con) -> int {
  std::cout << "Connect to: " + host + ":" + std::to_string(port) << std::endl;
  if (runtime_ != nullptr) {
    runtime_->getLogger(INFO)
        << "Connect to: " + host + ":" + std::to_string(port);
  }

  return connectViaHost(host, port, con.get());
}

auto Client::connectViaHost(const std::string &host, unsigned short port, Connection *con) -> int {
  server_addr_.sin_family = AF_INET;
  server_addr_.sin_port = htons(port);

  if (inet_pton(AF_INET, host.c_str(), &server_addr_.sin_addr) <= 0) {
    if (runtime_ != nullptr) {
      runtime_->getLogger(ERROR) << "Invalid address_ / Address not unknown";
    }

    std::cerr << "Invalid address_ / Address not unknown" << std::endl;
    return -1;
  }

  if (connect(
          this_socket_,
          (struct sockaddr *)&server_addr_,  // NOLINT this cast is necessary
          sizeof(server_addr_)) < 0) {
    if (runtime_ != nullptr) {
      runtime_->getLogger(ERROR) << "Connection to other server failed";
    }

    std::cerr << "Connection to other server failed" << std::endl;
    return -1;
  }
  con->socket = this_socket_;
  con->address = host + ":" + std::to_string(port);
  con->ignore = false;
  con->client_obj = this;

  return 0;
}

auto Client::bindToAddr() -> int {
    return -1;
}

void Client::autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                         const std::shared_ptr<internal_essential::json> &return_json) {
    std::string command = json_data->at("command");
    if (command == "host") {
        (*return_json)["data"] = {{"host", host_}};
    } else if (command == "port") {
        (*return_json)["data"] = {{"port", port_}};
    }
}

auto Client::hash() -> unsigned int {
    internal_essential::MurmurHash murmur_hash{};
    murmur_hash.Begin();
    murmur_hash.Add(reinterpret_cast<const unsigned char *>(host_.c_str()), host_.size());
    return murmur_hash.End() + std::time(nullptr) % 300;
}

void Client::addToRuntime() {
    if (runtime_ != nullptr) {
    unsigned int hash = this->hash();
    objects_[hash] = std::shared_ptr<Client>(this);
  }
}
