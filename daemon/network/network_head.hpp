//
// Created by revol-xut on 12/8/19.
//

#ifndef RUSSEL_NETWORK_HEAD_HPP
#define RUSSEL_NETWORK_HEAD_HPP

#include <chrono>
#include <condition_variable>
#include <iostream>
#include <memory>
#include <queue>
#include <thread>

#include "../config/config.hpp"
#include "../information_object.hpp"
#include "container/client_container.hpp"
#include "container/server_container.hpp"
#include "socket_handler.hpp"

constexpr unsigned int kIDNetworkHead = 0;

/*!
 * @brief This class manages all communication services (IPC Server, Pub Server,
 * and client connections to other russel services.
 */
class NetworkHead : public internal_essential::InformationObject {
public:

    NetworkHead();

    /*!
     * @brief Constructs Object with a choice of what connection types should be enabled
     */
    NetworkHead(bool ipc, bool pub, bool clients);

    ~NetworkHead();

    /*!
     * @brief Checks for new Connections and new Messages
     * @param messages Pointer where the new messages will be put
     */
    void checkEveryThing(const std::shared_ptr<MessageList> &messages);

    /*!
     * @brief Sends a stack of messages. Only allowed to be called from main
     * loop because of sync
     * @param messages
     */
    void send(const std::shared_ptr<MessageList> &messages);
    /*!
     * @brief Sends single messages to right place if socket manager is loaded
     * @param message
     */
    void send_single_message(const std::shared_ptr<RawMessage> &message);
    /*!
     * @brief Adds Message to message send queue so there are no fucked up
     * messages from different threads writing the socket at the same time
     * @param raw_message
     */
    void merge_send_back(const std::shared_ptr<RawMessage> &raw_message);

    void autoRespond(
        const std::shared_ptr<internal_essential::json> &json_data,
        const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

   private:
    void autoConnect();

    void send_back_loop();

    // Marks if the communication way is enabled
    bool ipc_ = false, pub_ = false, cli_ = false;
    std::shared_ptr<ServerContainer> server_ipc_;
    std::shared_ptr<ServerContainer> server_pub_;
    std::shared_ptr<ClientContainer> client_pub_;

    std::queue<std::shared_ptr<RawMessage>> queue_;
    bool send_back_loop_running_;

    std::mutex mutex_;
    std::condition_variable cv_;

    std::unique_ptr<std::thread> thread_;

    unsigned int additional_tasks_ = 0;

    std::string data_;
};


#endif //RUSSEL_NETWORK_HEAD_HPP
