//
// Created by revol-xut on 11/9/19.
//

#ifndef WORKER_IPC_SERVER_HPP
#define WORKER_IPC_SERVER_HPP

#include "socket.hpp"
#include "../information_object.hpp"

constexpr int kConnectionIpcCache = 5;

class IPCServer : public ServerSocket {
public:
    /*!
     * @brief Uses default 'ipc' value from config to create the Unix Domain Socket
     */
    explicit IPCServer();

    /*!
     * @brief Uses given value to create the Unix Domain Socket
     * @param sock_path
     */
    explicit IPCServer(const std::string &sock_path);

    /*!
     * @brief default destructor
     */
    ~IPCServer();

    /*!
     * @brief accepts incoming connection.
     * @return Returns Connection struct
     */
    auto acceptConnection() -> std::shared_ptr<Connection> override;

    /*!
     * @brief listens for incoming connections
     */
    void listenConnection() override;

    void autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                     const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

private:

    /*!
     * @brief Bind Socket too defined values
     * @return success status 0 or failed 1
     */
    auto bindToAddr() -> int override;

    struct sockaddr_un address_{};

    std::string unix_domain_socket_;

};


#endif //WORKER_IPC_SERVER_HPP
