//
// Created by worker on 12/7/19.
//

#ifndef RUSSEL_CLIENT_CONTAINER_HPP
#define RUSSEL_CLIENT_CONTAINER_HPP

#include "../client.hpp"
#include "../socket_handler.hpp"

class ClientContainer : public SocketWatcher {
public:
    ClientContainer();

    ~ClientContainer();

    /*!
     * @brief Creates new connection too an other russel engine
     * @param host Host e.g "127.0.0.1"
     * @param port Port e.g 8321
     * @return status 0 ok -1 failed
     */
    auto connectToEngine(const std::string &host, unsigned short port) -> int;

    /*!
     * @brief Returns client with this index
     * @param index unsigned value which is the index
     * @return Pointer to Client object
     */
    auto getClient(unsigned index) -> const std::shared_ptr<Client> &;

    /*
     * @brief Returns size of the client carrying container
     */
    auto getSize() -> unsigned;

    void autoRespond(
        const std::shared_ptr<internal_essential::json> &json_data,
        const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

    void send_message(const std::shared_ptr<RawMessage> &message);

   private:
    std::vector<std::shared_ptr<Client>> other_engines_;
};


#endif //RUSSEL_CLIENT_CONTAINER_HPP
