//
// Created by worker on 12/7/19.
//

#include "client_container.hpp"

#include <nlohmann/json.hpp>

ClientContainer::ClientContainer() {
  setIdentity(kCLIENT);

  // Adds the file descriptors to the set
  FD_ZERO(&socket_set_);  // NOLINT
  addToRuntime();
}

ClientContainer::~ClientContainer() = default;

auto ClientContainer::connectToEngine(const std::string &host,
                                      unsigned short port) -> int {
  auto ptr = std::make_shared<Client>();
  auto con = std::make_shared<Connection>();
  int status = ptr->connectViaHost(host, port, con);

  if (status != 0 && runtime_ != nullptr) {
    runtime_->getLogger(DEBUG)
        << "Client Container could not connect to: " + host + ":" +
               std::to_string(port);
    return -1;
  }
  FD_SET(con->socket, &socket_set_);  // NOLINT

  appendConnection(con);

  ptr->set_valid(con->socket);
  other_engines_.push_back(ptr);

  if (runtime_ != nullptr and objects_.find(22) != objects_.end() and
      con->socket > 0) {  // TODO: make sure this on of the last method to
                          // be called when application is constructred.
    auto input_json = std::make_shared<nlohmann::json>();
    (*input_json)["command"] = "register";
    (*input_json)["socket_id"] = con->socket;
    (*input_json)["socket_origin"] = kCLIENT;
    (*input_json)["is_engine"] = true;
    auto return_json = std::make_shared<nlohmann::json>();
    objects_.at(22)->autoRespond(input_json, return_json);
  }

  return con->socket;
}

auto ClientContainer::getClient(unsigned index)
    -> const std::shared_ptr<Client> & {
  return other_engines_.at(index);
}

auto ClientContainer::getSize() -> unsigned { return other_engines_.size(); }

void ClientContainer::autoRespond(
    const std::shared_ptr<internal_essential::json> &json_data,
    const std::shared_ptr<internal_essential::json> &return_json) {
  std::string command = json_data->at("command");

  if (command == "connection_count") {
    (*return_json)["data"] = {{"socket_count", other_engines_.size()}};
  } else if (command == "get_connection") {
  }
}

auto ClientContainer::hash() -> unsigned int { return kCLIENT + 1; }

void ClientContainer::addToRuntime() {
  if (runtime_ != nullptr) {
    objects_[hash()] = std::shared_ptr<ClientContainer>(this);
  }
}

void ClientContainer::send_message(const std::shared_ptr<RawMessage> &message) {
  int socket = message->socket;
  for (const std::shared_ptr<Client> &con : other_engines_) {
    if (con->getSocket() == socket and con->checkValidity(socket)) {
      con->sendSmartPointer(message);
      return;
    }
  }
}