//
// Created by revol-xut on 12/7/19.
//

#ifndef RUSSEL_SERVER_CONTAINER_HPP
#define RUSSEL_SERVER_CONTAINER_HPP

#include "../socket_handler.hpp"

class ServerContainer : public SocketWatcher {
public:

    /*!
     * @brief Creates the Public and Unix Domain socket
     */
    explicit ServerContainer(bool);

    /*!
     * @brief Default destructor
     */
    ~ServerContainer();


    /*!
     * @brief Checks for new incoming connections and accepts them
     */
    void checkConnections();


    auto getServer() -> std::shared_ptr<ServerSocket>;

    void setServer(const std::shared_ptr<ServerSocket> &socket);

    void autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                     const std::shared_ptr<internal_essential::json> &return_json) override;

    auto hash() -> unsigned int override;

    void addToRuntime() override;

private:

    std::shared_ptr<ServerSocket> my_socket_;

    int sock_max_ = 0;
    int socket_count_ = 0;
};


#endif //RUSSEL_SERVER_CONTAINER_HPP
