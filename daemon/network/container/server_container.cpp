//
// Created by revol-xut on 12/7/19.
//

#include "server_container.hpp"

#include <utility>


ServerContainer::ServerContainer(bool ipc) {
    /*
     * Uses default values from config_
     */
    if (ipc) {
        setServer(std::make_shared<IPCServer>());
        setIdentity(kIpc);
    } else {
        setServer(std::make_shared<PublicServer>());
        setIdentity(kPUBLIC);
    }
    sock_max_ = my_socket_->getSocket();

    // Adds the two file descriptors to the set
    FD_ZERO(&socket_set_); //NOLINT
    FD_SET(my_socket_->getSocket(), &socket_set_); //NOLINT
    my_socket_->listenConnection();
    setSocket(my_socket_);
    addToRuntime();
}

ServerContainer::~ServerContainer() = default;


void ServerContainer::checkConnections() {
    timeval timeout{0, kTimeout};

    // Updates set
    socket_read_ = socket_set_;
    int iterations = 0; // Security Mechanism

    for (;;) {
      int ready =
          select(sock_max_ + 1, &socket_read_, nullptr, nullptr, &timeout);
      // todo: Check if FD_SETSIZE is relevant if this is the case we need more
      // sets too handle the connections_

      if (ready > 0) {
        if (FD_ISSET(my_socket_->getSocket(), &socket_read_)) {  // NOLINT
          // Checks for new incoming connection
          auto connection = my_socket_->acceptConnection();

          if (connection->socket > sock_max_) {
            sock_max_ = connection->socket;
            // Select need the highest number of file descriptors
          }
          FD_SET(connection->socket, &socket_set_);  // NOLINT Experimental

          if (connection->socket >= 0) {
            appendConnection(connection);
            std::cout << "Accepted Connection " + connection->address
                      << std::endl;
            if (runtime_ != nullptr) {
              runtime_->getLogger(INFO)
                  << "Accepted Connection " + connection->address;
            }
          }
        }
      }

      if (ready <= 0 or ready - iterations <= 0) {
        break;
      }
      iterations++;
    }
}

void ServerContainer::autoRespond(const std::shared_ptr<internal_essential::json> &json_data,
                                  const std::shared_ptr<internal_essential::json> &return_json) {
    std::string command = json_data->at("command");
    if (command == "connection_count") {
        (*return_json)["data"]["socket_count"] = getConnectionCount();
    } else if (command == "delete_socket") {
        findAndErase((*json_data)["data"]["socket"]);
    } else if (command == "send_messages") {
        int socket = (*json_data)["socket"];
        unsigned short origin = (*json_data)["origin"];
        std::string data = (*json_data)["data"];
        unsigned int size = (*json_data)["size"];

        RawMessage raw_message{data.c_str(), size, socket, origin};

        this->routeAndSend(&raw_message);

    }
}

auto ServerContainer::hash() -> unsigned int {
    return getIdentity() + 1;
}

void ServerContainer::addToRuntime() {
    if (runtime_ != nullptr) {
        objects_[hash()] = std::shared_ptr<ServerContainer>(this);
    }
}

std::shared_ptr<ServerSocket> ServerContainer::getServer() {
    return my_socket_;
}

void ServerContainer::setServer(const std::shared_ptr<ServerSocket> &socket) {
    my_socket_ = socket;
}